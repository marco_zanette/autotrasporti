<!-- Bootstrap -->
<link href="<?php echo env('APP_URL')?>/tema/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo env('APP_URL')?>/tema/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="<?php echo env('APP_URL')?>/tema/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

<!-- Custom Theme Style -->
<link href="<?php echo env('APP_URL')?>/tema/build/css/custom.min.css" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<?php echo env('APP_URL')?>/css/main.css" rel="stylesheet">
<!-- Responsive table -->
<link href="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
