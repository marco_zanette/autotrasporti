
@extends('layouts.main')



@section('content_page')


    <h1>Nuovo viaggio</h1>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_veicolo" name="id_veicolo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Veicolo::all() as $veicolo){
                                        ?>
                                        <option value="{{ $veicolo->id }}">{{ $veicolo->targa }} - {{ $veicolo->nome }}</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rimorchio</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_rimorchio" name="id_rimorchio" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Rimorchio::orderBy("targa")->get() as $rimorchio){
                                    ?>
                                    <option value="{{ $rimorchio->id }}">{{ $rimorchio->targa }} - {{ $rimorchio->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Azienda</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}">{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Partenza</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="p_luogo" name="p_luogo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::orderBy('nome', 'asc')->get() as $luogo){
                                    ?>
                                    <option value="{{ $luogo->id }}">{{ $luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="">
                                    <input type="text" class="form-control has-feedback-left" id="p_data" name="p_data" placeholder="" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" ></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Km</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control" type="number" name="p_km" id="p_km" />
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Ora</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="p_ora" name="p_ora" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>





                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Partenza</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="a_luogo" name="a_luogo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::orderBy('nome', 'asc')->get() as $luogo){
                                    ?>
                                    <option value="{{ $luogo->id }}">{{ $luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="">
                                    <input type="text" class="form-control has-feedback-left" id="a_data" name="a_data" placeholder="" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" ></span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Km</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control" type="number" name="a_km" id="a_km" />
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Ora</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="a_ora" name="a_ora" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection







@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>

    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>
        $(document).ready(function() {

            $('#p_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#a_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });


            $("#id_veicolo").select2({
                placeholder: "Seleziona veicolo",
                allowClear: true
            });

            $("#id_rimorchio").select2({
                placeholder: "Seleziona rimorchio",
                allowClear: true
            });

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#p_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });

            $("#a_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });
        });
    </script>
    <!-- /Select2 -->


@endsection
