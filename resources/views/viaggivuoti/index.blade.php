
@extends('layouts.main')



@section('content_page')


    <h1> Viaggi</h1>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista dei viaggi</h2>
                <ul class="add_all nav navbar-right panel_toolbox">
                    <li>
                        <a href="<?php echo route('viaggivuoti::add')?>" class="btn btn-primary">Aggiungi un nuovo viaggio<i class="fa fa-plus"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Azienda</th>
                        <th>Veicolo</th>
                        <th>Rimorchio</th>
                        <th>Partenza</th>
                        <th>KM</th>
                        <th>Ora</th>
                        <th>Arrivo</th>
                        <th>KM</th>
                        <th>Ora</th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($viaggi as $viag){

                    ?>
                    <tr >
                        <td>
                            <?php echo $viag->id; ?>
                                <div class="modal fade" id="ModalConfirmDelete_{{$viag->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" style="color: #f00; font-size: 20px;">Attenzione</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Sei sicuro di voler cancellare l'elemento selezionato?
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                                                <a href="<?php echo route('viaggivuoti::delete',['id' => $viag->id ]) ?>" class="btn btn-danger">Si, cancella</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </td>
                        <td>
                            @if($viag->azienda)
                                {{ $viag->azienda->ragsoc }}
                            @endif
                        </td>
                        <td>
                            @if($viag->veicolo)
                                {{ $viag->veicolo->targa }} - {{ $viag->veicolo->nome }}
                            @endif
                        </td>
                        <td>
                            @if($viag->rimorchio)
                                {{ $viag->rimorchio->targa }} - {{ $viag->rimorchio->nome }}
                            @endif
                        </td>
                        <td>
                            @if($viag->partenza)
                                {{ $viag->partenza->nome }}
                            @endif
                        </td>
                        </td>
                        <td>{{ $viag->p_km }}</td>
                        <td>{{ $viag->p_data }} {{ $viag->p_ora }}</td>
                        <td>
                            @if($viag->arrivo)
                                {{ $viag->arrivo->nome }}
                            @endif
                        </td>
                        </td>
                        <td>{{ $viag->a_km }}</td>
                        <td>{{ $viag->a_data }} {{ $viag->a_ora }}</td>

                        <td>
                            <a href="<?php echo route('viaggivuoti::edit',['id' => $viag->id])?>" class="btn btn-dark">Modifica</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-target="#ModalConfirmDelete_{{$viag->id}}" data-toggle="modal" >Elimina</button>
                        </td>
                    </tr>

                    <?php

                    }
                    ?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>






@endsection





@section('footer_script')
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable( {
                "order": [[ 0, 'desc' ]]
            } );


        });
    </script>
    <!-- /Datatables -->


@endsection