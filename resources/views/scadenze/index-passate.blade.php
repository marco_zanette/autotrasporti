
@extends('layouts.main')



@section('content_page')


    <h1> Scadenze passate</h1>
    <?php

    \App\Utilities\AlertMsg::stampaMsg();

    ?>



    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista scadenze passate</h2>
                <ul class="add_all nav navbar-right panel_toolbox">

                </ul>

                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Titolo</th>
                        <th>Importo</th>
                        <th></th>
                        <th width="100"></th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    foreach ($scadenze->all() as $scad){

                        $class = "";
                        if($scad['tipo'] == 'entrata'){
                            $class = "bg-green";
                        }
                        if($scad['tipo'] == 'uscita'){
                            $class = "bg-orange";
                        }

                        $rag_soc_entita = "";


                        switch ($scad['entita']){
                            case "rifornimento":
                                $entitaOBJ = \App\Models\Rifornimento::find($scad['id']);
                                $aziendaEntitaOBJ = \App\Models\Luogo::find($entitaOBJ->id_luogo);
                                if($aziendaEntitaOBJ){
                                    $rag_soc_entita = $aziendaEntitaOBJ->nome;
                                }
                                break;
                            case "fattura":
                                $entitaOBJ = \App\Models\Fattura::find($scad['id']);
                                $aziendaEntitaOBJ = \App\Models\Azienda::find($entitaOBJ->id_azienda);
                                if($aziendaEntitaOBJ){
                                    $rag_soc_entita = $aziendaEntitaOBJ->ragsoc;
                                }
                                break;
                            case "fattura_acquisto":
                                $entitaOBJ = \App\Models\FatturaAcquisto::find($scad['id']);
                                $aziendaEntitaOBJ = \App\Models\Azienda::find($entitaOBJ->id_azienda);
                                if($aziendaEntitaOBJ){
                                    $rag_soc_entita = $aziendaEntitaOBJ->ragsoc;
                                }
                                break;
                        }


                    ?>
                    <tr class="{{ $class }}">
                        <td>
                            <?php
                            $dataOBJ = new \Carbon\Carbon($scad['data']);
                            echo $dataOBJ->format('d/m/Y');
                            ?>
                        </td>
                        <td><?php echo $scad['titolo']; ?></td>
                        <td>
                            € <?php echo $scad['importo']; ?>
                        </td>
                        <td>
                            <?php
                            if($scad['tipo'] == 'uscita'){
                                echo "creditore: ".$rag_soc_entita;
                            } else {
                                echo "debitore: ".$rag_soc_entita;
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if($scad['entita'] == "rifornimento"){
                                ?>
                                <a href="<?php echo route('rifornimenti::edit',['id' => $scad['id']])?>" class="btn btn-dark">Dettagli</a>
                                <?php
                            }
                            ?>


                            <?php
                            if($scad['entita'] == "fattura"){
                            ?>
                            <a href="<?php echo route('fatture::edit',['id' => $scad['id']])?>" class="btn btn-dark">Dettagli</a>
                            <?php
                            }
                            ?>



                            <?php
                            if($scad['entita'] == "fattura_acquisto"){
                            ?>
                            <a href="<?php echo route('fattureacquisto::edit',['id' => $scad['id']])?>" class="btn btn-dark">Dettagli</a>
                            <?php
                            }
                            ?>

                        </td>
                    </tr>

                    <?php
                    }
                    ?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>






@endsection





@section('footer_script')
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable();


        });
    </script>
    <!-- /Datatables -->


@endsection