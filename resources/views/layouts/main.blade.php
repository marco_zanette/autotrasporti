<!DOCTYPE html>
<html lang="it">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gestionale autotrasporti</title>
    @include('elements.script_header')
</head>
<body class="nav-md">

    <div class="container body">
        <div class="main_container">


            @include('elements.menu')
            {{--
            @include('elements.sidebar_menu')
            --}}

            <!-- Page content-->
            <div class="right_col" role="main">
                <div class="">
                    @yield('content_page')

                </div>
            </div>





            <!-- Page footer-->
            <footer>

                @include('elements.footer')

            </footer>


    </div>
</div>

    <!-- jQuery -->
    <script src="<?php echo env('APP_URL')?>/tema/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo env('APP_URL')?>/tema/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo env('APP_URL')?>/tema/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo env('APP_URL')?>/tema/vendors/nprogress/nprogress.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="<?php echo env('APP_URL')?>/tema/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo env('APP_URL')?>/tema/build/js/custom.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>


    <!-- jquery.inputmask -->
    <script>
        $(document).ready(function() {
            $(":input").inputmask();
        });
    </script>
    <!-- /jquery.inputmask -->

    @yield('footer_script')

</body>
</html>