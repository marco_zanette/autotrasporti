
@extends('layouts.main')



@section('content_page')


    <h1>Nuova fattura</h1>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" ng-app="FattureApp">

                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content" ng-controller="GestisciFattura">
                    <br />
                    <form novalidate id="form_fattura" data-parsley-validate class="form-horizontal form-label-left" method="post" >

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="data" name="data" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Azienda</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}">{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <label class="control-label col-xs-12" style="text-align: center;">Viaggi</label>
                            <br />
                            <br />
                            <div class="row" ng-repeat="viaggio in viaggi track by $index" style="border-bottom: solid 1px #E6E9ED; padding-bottom: 5px; margin-bottom: 5px;">

                                <div class="col-xs-5 col-md-offset-2">
                                    @{{ viaggio.data}}
                                    <br />
                                    @{{ viaggio.luogo_carico }} - Km @{{ viaggio.km_carico }} - @{{ viaggio.ora_carico }}
                                    <br />
                                    @{{ viaggio.luogo_scarico }} - Km @{{ viaggio.km_scarico }} - @{{ viaggio.ora_scarico }}
                                </div>

                                <div class="col-xs-2">
                                    <input ng-model="viaggio.prezzo" type="text" class="form-control has-feedback-left">
                                    <span class="fa fa-euro form-control-feedback left" aria-hidden="true"></span>
                                </div>

                                <div class="col-xs-1">
                                    <button type="button" class="btn btn-danger" ng-click="remove($index)">
                                        <span class="fa fa-trash" aria-hidden="true"></span>
                                    </button>
                                </div>


                            </div>

                            <div class="col-xs-12" style="text-align: center;">
                                <button class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">
                                    Seleziona viaggio
                                </button>
                            </div>

                            <br /><br />
                            <br />

                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Imponibile</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input ng-value="imponibile()" type="number" step="any" id="imponibile" name="imponibile" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">% IVA applicata</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input ng-model="iva_applicata" type="number" id="iva_applicata" name="iva_applicata" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">IVA</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input ng-value="iva()" type="text" id="iva" name="iva" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Totale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input ng-value="totale()" type="text" id="totale" name="totale" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Descrizione</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="descrizione"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo riferimento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="tipo_riferimento" name="tipo_riferimento" class="form-control" tabindex="-1">
                                    <option value="vs. ordine">vs. ordine</option>
                                    <option value="ns. contratto">ns. contratto</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Numero riferimento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="numero_riferimento" name="numero_riferimento" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 1</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="scadenza1" id="scadenza1" class="form-control active" value="">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input type="text" id="scadenza1_importo" name="scadenza1_importo" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 2</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="scadenza2" id="scadenza2" class="form-control active" value="">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input type="text" id="scadenza2_importo" name="scadenza2_importo" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 3</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" name="scadenza3" id="scadenza3" class="form-control active" value="">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input type="text" id="scadenza3_importo" name="scadenza3_importo" class="form-control" />
                            </div>
                        </div>


                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success" ng-click="submit()" >Aggiungi</button>
                            </div>
                        </div>

                        {{ csrf_field() }}


                        <input type="hidden" name="viaggi" value="@{{ viaggi }}" />


                        <div id="modal_viaggi" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; z-index: 99999;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Seleziona il viaggio</h4>
                                    </div>
                                    <div class="modal-body">

                                        <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Giorno</th>
                                                <th>Luogo carico</th>
                                                <th>KM</th>
                                                <th>Ora arrivo</th>
                                                <th>Documento</th>
                                                <th>Luogo scarico</th>
                                                <th>KM</th>
                                                <th>Ora arrivo</th>
                                                <th>Fatturare a</th>
                                                <th style="width: 80px;"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $array_viaggi_sel = [];
                                            $index = 0;
                                            foreach (\App\Models\Viaggio::all() as $viag){


                                            // controllo se già in qualche fattura
                                            $qCheck = "SELECT * FROM fatture WHERE viaggi LIKE '%\"id\":".$viag->id.",%' ";
                                            $risCheck = \DB::select($qCheck);
                                            if(count($risCheck) > 0){
                                                continue;
                                            }


                                            $veicoloOBJ = \App\Models\Veicolo::find($viag->id_veicolo);

                                            $rimorchioOBJ = \App\Models\Rimorchio::find($viag->id_rimorchio);

                                            $rag_soc = "";
                                            if($viag->id_azienda != 0){
                                                $aziendaOBJ = \App\Models\Azienda::find($viag->id_azienda);
                                                $rag_soc = $aziendaOBJ->ragsoc;
                                            }


                                            $giorno = "";
                                            $luogo = "";
                                            $km = "";
                                            $ora = "";
                                            $caricoOBJ = \App\Models\Carico::where("id_viaggio",$viag->id)->first();
                                            if($caricoOBJ){
                                                $giorno = $caricoOBJ->data;
                                                $km = $caricoOBJ->km;
                                                $luogo = "";
                                                $ora = formatOra($caricoOBJ->ora_arrivo);
                                                if($caricoOBJ->id_luogo != 0){
                                                    $luogoOBJ = \App\Models\Luogo::find($caricoOBJ->id_luogo);
                                                    $luogo = $luogoOBJ->nome;

                                                    $aziendaLuogo = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                                                    if($aziendaLuogo){
                                                        $luogo = $aziendaLuogo->ragsoc." ".$luogo;
                                                    }
                                                }
                                            }



                                            // scarico
                                            $luogo_s = "";
                                            $km_s = "";
                                            $ora_s = "";
                                            $scaricoOBJ = \App\Models\Scarico::where("id_viaggio",$viag->id)->first();
                                            if($scaricoOBJ){
                                                $km_s = $scaricoOBJ->km;
                                                $luogo_s = "";
                                                $ora_s = formatOra($scaricoOBJ->ora_arrivo);
                                                if($scaricoOBJ->id_luogo != 0){
                                                    $luogoOBJ = \App\Models\Luogo::find($scaricoOBJ->id_luogo);
                                                    $luogo_s = $luogoOBJ->nome;

                                                    $aziendaLuogoS = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                                                    if($aziendaLuogoS){
                                                        $luogo_s = $aziendaLuogoS->ragsoc." ".$luogo_s;
                                                    }
                                                }
                                            }



                                            $rimorchio = "";
                                            if($viag->id_rimorchio != 0){
                                                $rimorchioOBJ = \App\Models\Rimorchio::find($viag->id_rimorchio);
                                                $rimorchio = $rimorchioOBJ->nome;
                                            }

                                            $veicolo = "";
                                            if($viag->id_veicolo != 0){
                                                $veicoloOBJ = \App\Models\Veicolo::find($viag->id_veicolo);
                                                $veicolo = $veicoloOBJ->nome;
                                            }

                                            $array_viaggi_sel[] = [
                                                'id' => $viag->id,
                                                'data' => $giorno,
                                                'luogo_carico' => $luogo,
                                                'ora_carico' => $ora,
                                                'km_carico' => $km,
                                                'luogo_scarico' => $luogo_s,
                                                'ora_scarico' => $ora_s,
                                                'km_scarico' => $km_s,
                                                'veicolo' => $veicolo,
                                                'rimorchio' => $rimorchio,
                                                'azienda' => $rag_soc,
                                                'prezzo' => $viag->prezzo,
                                            ];

                                            ?>
                                            <tr>
                                                <td><?php echo $viag->numero; ?></td>
                                                <td>{{$giorno}}</td>
                                                <td>{{ $luogo }}</td>
                                                <td>{{ $km }}</td>
                                                <td>{{ $ora }}</td>
                                                <td>
                                                    <?php
                                                    if($viag->riferimento_doc != ""){
                                                    ?>
                                                    <a href="{{ $viag->riferimento_doc }}" target="_blank">
                                                        <img <?php echo "src='".$viag->riferimento_doc."'"; ?> style="max-height:100px; max-width: 100px;">
                                                    </a>
                                                    <?php
                                                    }
                                                    ?>
                                                </td>

                                                <td>
                                                    {{ $luogo_s }}
                                                </td>
                                                <td>{{ $km_s }}</td>
                                                <td>{{ $ora_s }}</td>


                                                <td><?php echo $rag_soc; ?></td>
                                                    <td>
                                                        <input type="checkbox" value="{{$index}}" class="sel_add_viaggi" >
                                                    </td>
                                                </tr>

                                            <?php
                                                $index++;
                                            }
                                            ?>

                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="addSelezionati()">Aggiungi selezionati</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')


    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable();


        });
    </script>
    <!-- /Datatables -->



    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>

    <script src="<?php echo env('APP_URL')?>/js/angular/fatture.js"></script>


    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>
        viaggi = [];

        iva_applicata = 0;

        viaggi_sel = JSON.parse('<?php echo addslashes(json_encode($array_viaggi_sel,JSON_HEX_QUOT | JSON_HEX_APOS)); ?>');


        $(document).ready(function() {

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });


            $('#scadenza1').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza1').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });


            $('#scadenza2').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

            $('#scadenza3').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza3').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

        });
    </script>
    <!-- /Select2 -->


@endsection


