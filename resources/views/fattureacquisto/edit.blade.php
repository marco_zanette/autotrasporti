
@extends('layouts.main')



@section('content_page')


    <h1>Modifica fattura</h1>

    <?php

    \App\Utilities\AlertMsg::stampaMsg();

    ?>



    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" >

                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content" ng-controller="GestisciFattura">
                    <br />
                    <form novalidate id="form_fattura" data-parsley-validate class="form-horizontal form-label-left" method="post" >

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php
                                $data_ok = new \Carbon\Carbon($fattura->data);
                                ?>
                                <input value="{{ $data_ok->format("d/m/Y") }}" type="text" id="data" name="data" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Azienda</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::all() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" <?php if($azienda->id == $fattura->id_azienda) echo "selected"; ?>>{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Numero fattura</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="numero" name="numero" value="{{ $fattura->numero }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Imponibile</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $fattura->imponibile }}" type="number" step="any" id="imponibile" name="imponibile" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">% IVA applicata</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $fattura->iva_applicata }}" type="number" step="any" id="iva_applicata" name="iva_applicata" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">IVA</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $fattura->iva }}" type="number" step="any" id="iva" name="iva" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Totale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $fattura->totale }}" type="number" step="any" id="totale" name="totale" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 1</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <?php
                                    $data_scad1 = "";
                                    if($fattura->scadenza1){
                                        $data_scad1 = new \Carbon\Carbon($fattura->scadenza1);
                                        $data_scad1 = $data_scad1->format("d/m/Y");
                                    }
                                    ?>
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input value="{{ $data_scad1 }}" type="text" name="scadenza1" id="scadenza1" class="form-control active" >
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input value="{{ $fattura->scadenza1_importo }}" type="text" id="scadenza1_importo" name="scadenza1_importo" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 2</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <?php
                                    $data_scad2 = "";
                                    if($fattura->scadenza2){
                                        $data_scad2 = new \Carbon\Carbon($fattura->scadenza2);
                                        $data_scad2 = $data_scad2->format("d/m/Y");
                                    }
                                    ?>
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input value="{{ $data_scad2 }}" type="text" name="scadenza2" id="scadenza2" class="form-control active">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input value="{{ $fattura->scadenza2_importo }}" type="text" id="scadenza2_importo" name="scadenza2_importo" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Scadenza 3</label>
                            <div class="col-xs-3 -align-center">
                                <div class="input-prepend input-group">
                                    <?php
                                    $data_scad3 = "";
                                    if($fattura->scadenza2){
                                        $data_scad3 = new \Carbon\Carbon($fattura->$data_scad3);
                                        $data_scad3 = $data_scad3->format("d/m/Y");
                                    }
                                    ?>
                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input value="{{ $data_scad3 }}" type="text" name="scadenza3" id="scadenza3" class="form-control active">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <input value="{{ $fattura->scadenza3_importo }}" type="text" id="scadenza3_importo" name="scadenza3_importo" class="form-control" />
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Riferimento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">


                                <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                    <input id="thumbnail" class="form-control" type="text" name="filepath" value="{{ $fattura->documento }}">
                                </div>
                                <img <?php if($fattura->documento) echo "src='".$fattura->documento."'"; ?> id="holder" style="margin-top:15px;max-height:100px;">

                                <input type="button" class="btn btn-default" onclick="$('#thumbnail').val(''); $('#holder').attr('src','');" value="Elimina foto scelta" />

                            </div>
                        </div>


                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success" ng-click="submit()" >Salva</button>
                            </div>
                        </div>

                        {{ csrf_field() }}




                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')


    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable();


        });
    </script>
    <!-- /Datatables -->



    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>

    <script src="<?php echo env('APP_URL')?>/js/angular/fatture.js"></script>


    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script>


        $(document).ready(function() {

            $('#lfm').filemanager('image');

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });


            $('#scadenza1').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza1').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });


            $('#scadenza2').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

            $('#scadenza3').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });
            $('#scadenza3').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

        });
    </script>
    <!-- /Select2 -->


@endsection


