
@extends('layouts.main')



@section('content_page')


    <h1>Nuovo ordine</h1>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" ng-app="FattureApp">

                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content" ng-controller="GestisciFattura">
                    <br />
                    <form novalidate id="form_fattura" data-parsley-validate class="form-horizontal form-label-left" method="post" >

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="data" name="data" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Numero</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="numero" name="numero" value="0" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        -->

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Vettore</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}">{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Prezzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="prezzo" name="prezzo" value="0" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo carico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo_carico" name="id_luogo_carico" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                    $rag_soc = "";
                                    if($luogo->id_azienda != 0){
                                        $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                                        $rag_soc = $aziendaOBJ->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $luogo->id }}" >{{ $rag_soc." - ".$luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo scarico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo_scarico" name="id_luogo_scarico" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                    $rag_soc = "";
                                    if($luogo->id_azienda != 0){
                                        $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                                        $rag_soc = $aziendaOBJ->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $luogo->id }}" >{{ $rag_soc." - ".$luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Merce</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="merce" name="merce" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo carico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="carico" name="carico" class="form-control" tabindex="-1">
                                    <option value="PTL - Partial truck load">PTL - Partial truck load</option>
                                    <option value="FTL - Full truck load">FTL - Full truck load</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Metri lineari</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="ml" name="ml" value="0" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Metri cubi</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc" name="mc" value="0" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kg</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="kg" name="kg" value="0" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="veicolo" name="veicolo" class="form-control" tabindex="-1">
                                    <option value="Motrice centinata">Motrice centinata</option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">

                            <div class="col-md-3 col-sm-3 col-sm-offset-3 col-xs-12">
                                <label class="control-label col-xs-12">Carico data</label>
                                <input type="text" id="carico_data" name="carico_data" required="required" class="form-control col-md-7 col-xs-12">
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label class="control-label col-xs-12" for="first-name">Carico ora</label>
                                <input type="text" id="carico_ora" name="carico_ora" data-inputmask="'mask' : '99:99'" value="00:00"  required="required" class="form-control col-md-7 col-xs-12">
                            </div>

                        </div>





                        <div class="form-group">

                            <div class="col-md-3 col-sm-3 col-sm-offset-3 col-xs-12">
                                <label class="control-label col-xs-12">Scarico data</label>
                                <input type="text" id="scarico_data" name="scarico_data" required="required" class="form-control col-md-7 col-xs-12">
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label class="control-label col-xs-12" for="first-name">Scarico ora</label>
                                <input type="text" id="scarico_ora" name="scarico_ora" data-inputmask="'mask' : '99:99'" value="00:00"  required="required" class="form-control col-md-7 col-xs-12">
                            </div>

                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pagamento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="pagamento" name="pagamento" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>







                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success" >Aggiungi</button>
                            </div>
                        </div>

                        {{ csrf_field() }}



                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')


    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>





    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>


        $(document).ready(function() {

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#id_luogo_carico").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#id_luogo_scarico").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });

            $('#carico_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',

                },
            });

            $('#scarico_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });



            $('#scadenza1').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza1').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });


            $('#scadenza2').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

            $('#scadenza3').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza3').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

        });
    </script>
    <!-- /Select2 -->


@endsection


