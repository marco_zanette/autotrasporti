
@extends('layouts.main')



@section('content_page')


    <h1>Modifica ordini</h1>

    <?php

    \App\Utilities\AlertMsg::stampaMsg();

    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel" ng-app="FattureApp">

                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <br />
                    <form novalidate id="form_fattura" data-parsley-validate class="form-horizontal form-label-left" method="post" >

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php
                                $data_ok = new \Carbon\Carbon($ordine->data);
                                ?>
                                <input type="text" id="data" name="data" required="required" value="{{ $data_ok->format("d/m/Y") }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Numero</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="numero" name="numero" value="{{ $ordine->numero }}" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>
                        -->

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Vettore</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" <?php if($azienda->id == $ordine->id_azienda) echo "selected"; ?> >{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Prezzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="prezzo" name="prezzo" value="{{ $ordine->prezzo }}" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo carico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo_carico" name="id_luogo_carico" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                    $rag_soc = "";
                                    if($luogo->id_azienda != 0){
                                        $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                                        $rag_soc = $aziendaOBJ->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $luogo->id }}" <?php if($luogo->id == $ordine->id_luogo_carico) echo "selected"; ?>  >{{ $rag_soc." - ".$luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo scarico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo_scarico" name="id_luogo_scarico" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                    $rag_soc = "";
                                    if($luogo->id_azienda != 0){
                                        $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                                        $rag_soc = $aziendaOBJ->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $luogo->id }}" <?php if($luogo->id == $ordine->id_luogo_scarico) echo "selected"; ?>  >{{ $rag_soc." - ".$luogo->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Merce</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="merce" name="merce" value="{{ $ordine->merce }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo carico</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="carico" name="carico" class="form-control" tabindex="-1">
                                    <option value="PTL - Partial truck load" <?php if($ordine->carico == "PTL - Partial truck load") echo "selected"; ?> >PTL - Partial truck load</option>
                                    <option value="FTL - Full truck load" <?php if($ordine->carico == "FTL - Full truck load") echo "selected"; ?> >FTL - Full truck load</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Metri lineari</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="ml" name="ml" value="{{ $ordine->ml }}" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Metri cubi</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="mc" name="mc" value="{{ $ordine->mc }}" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Kg</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="kg" name="kg" value="{{ $ordine->kg }}" step="any" class="form-control col-md-7 col-xs-12" >
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tipo veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="veicolo" name="veicolo" class="form-control" tabindex="-1">
                                    <option value="Motrice centinata" <?php if($ordine->veicolo == "Motrice centinata") echo "selected"; ?> >Motrice centinata</option>
                                </select>
                            </div>
                        </div>



                        <div class="form-group">

                            <div class="col-md-3 col-sm-3 col-sm-offset-3 col-xs-12">
                                <label class="control-label col-xs-12">Carico data</label>
                                <?php
                                $data_ok_carico = new \Carbon\Carbon($ordine->carico_data);
                                ?>
                                <input type="text" id="carico_data" name="carico_data" value="{{ $data_ok_carico->format("d/m/Y") }}" class="form-control col-md-7 col-xs-12">
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label class="control-label col-xs-12" for="first-name">Carico ora</label>
                                <input type="text" id="carico_ora" data-inputmask="'mask' : '99:99'" name="carico_ora" value="{{ $ordine->carico_ora }}" class="form-control col-md-7 col-xs-12">
                            </div>

                        </div>





                        <div class="form-group">

                            <div class="col-md-3 col-sm-3 col-sm-offset-3 col-xs-12">
                                <label class="control-label col-xs-12">Scarico data</label>
                                <?php
                                $data_ok_scarico = new \Carbon\Carbon($ordine->scarico_data);
                                ?>
                                <input type="text" id="scarico_data" name="scarico_data" value="{{ $data_ok_scarico->format("d/m/Y") }}"  class="form-control col-md-7 col-xs-12">
                            </div>

                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label class="control-label col-xs-12" for="first-name">Scarico ora</label>
                                <input type="text" id="scarico_ora" data-inputmask="'mask' : '99:99'" name="scarico_ora" value="{{ $ordine->scarico_ora }}" class="form-control col-md-7 col-xs-12">
                            </div>

                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Pagamento</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="pagamento" name="pagamento" value="{{ $ordine->pagamento }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>







                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success" >Salva</button>
                            </div>
                        </div>

                        {{ csrf_field() }}



                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')


    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>





    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>


        $(document).ready(function() {

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#id_luogo_carico").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#id_luogo_scarico").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });

            $('#carico_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });

            $('#scarico_data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            });



            $('#scadenza1').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza1').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });


            $('#scadenza2').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza2').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

            $('#scadenza3').daterangepicker({
                autoUpdateInput: false,
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY'
                },
            });
            $('#scadenza3').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY'));
            });

        });
    </script>
    <!-- /Select2 -->


@endsection


