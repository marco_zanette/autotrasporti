
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gestionale </title>

    <!-- Bootstrap -->
    <link href="<?php echo env('APP_URL')?>/tema/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo env('APP_URL')?>/tema/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo env('APP_URL')?>/tema/build/css/custom.min.css" rel="stylesheet">
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form method="post">
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" class="form-control" placeholder="Username" required="" name="user" />
                    </div>
                    <div>
                        <input type="password" class="form-control" placeholder="Password" required="" name="pswd" />
                    </div>
                    <div>
                        <input class="btn btn-default submit" type="submit" value="login" />
                    </div>

                    <div class="clearfix"></div>

                    {{ csrf_field() }}

                </form>
            </section>
        </div>


    </div>
</div>
</body>
</html>