
@extends('layouts.main')



@section('content_page')


    <h1>Modifica luogo</h1>

    <?php

    \App\Utilities\AlertMsg::stampaMsg();

    ?>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nome" name="nome" value="{{$luogo->nome}}" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Indirizzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="indirizzo" name="indirizzo" value="{{$luogo->indirizzo}}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Comune</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="comune" name="comune" value="{{$luogo->comune}}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Cap</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cap" name="cap" value="{{$luogo->cap}}" class="form-control col-md-7 col-xs-12" data-inputmask="'mask':'99999'" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Provincia</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="provincia" name="provincia" value="{{$luogo->provincia}}" data-inputmask="'mask':'AA'"  class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nazione</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="nazione" name="nazione" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    foreach (\App\Models\Nazione::all()->sortBy('nome') as $naz){
                                    ?>
                                    <option value="{{$naz->iso}}" <?php if($naz->iso == $luogo->nazione) echo "selected"; ?> > {{$naz->nome}} </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Azienda</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" <?php if($luogo->id_azienda == $azienda->id) echo "selected"; ?>>{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <script>
        $(document).ready(function() {

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });
        });
    </script>
    <!-- /Select2 -->


@endsection


