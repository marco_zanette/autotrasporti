
@extends('layouts.main')



@section('content_page')


    <h1>Modifica Rifornimento</h1>
    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Modifica i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="">
                                    <?php
                                    $data_ok = new \Carbon\Carbon($rifornimenti->data);
                                    ?>
                                    <input type="text" value="{{ $data_ok->format("d/m/Y") }}" class="form-control has-feedback-left" id="data" name="data" placeholder="" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" ></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_veicolo" name="id_veicolo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Veicolo::all() as $veicolo){
                                    ?>
                                    <option value="{{ $veicolo->id }}" <?php if($rifornimenti->id_veicolo == $veicolo->id) echo "selected"; ?>>{{ $veicolo->nome }} {{ $veicolo->targa }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rimorchio</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_rimorchio" name="id_rimorchio" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Rimorchio::all() as $rimorchio){
                                    ?>
                                    <option value="{{ $rimorchio->id }}" <?php if($rifornimenti->id_rimorchio == $rimorchio->id) echo "selected"; ?>>{{ $rimorchio->nome }} {{ $rimorchio->targa }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        -->

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo" name="id_luogo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                        $luogo_azienda = "";

                                        if($luogo->id_azienda){
                                            $aziendaLuogo = \App\Models\Azienda::find($luogo->id_azienda);
                                            $luogo_azienda = $aziendaLuogo->ragsoc.",";
                                        }

                                    ?>
                                    <option value="{{ $luogo->id }}" <?php if($rifornimenti->id_luogo == $luogo->id) echo "selected"; ?>>{{$luogo_azienda}} {{ $luogo->nome }} - {{ $luogo->comune }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Km</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{$rifornimenti->km }}" type="number" id="km" name="km" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prodotto</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="prodotto" name="prodotto" class="form-control" >
                                    <option value="diesel" <?php if($rifornimenti->prodotto == 'diesel') echo 'selected'; ?>>Diesel</option>
                                    <option value="adblue" <?php if($rifornimenti->prodotto == 'adblue') echo 'selected'; ?>>Adblue</option>
                                    <option value="olio motore" <?php if($rifornimenti->prodotto == 'olio motore') echo 'selected'; ?>>Olio motore</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quantità</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input value="{{$rifornimenti->quantita }}" type="number" id="quantita" step="any" name="quantita" class="form-control col-md-7 col-xs-12">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <select name="quantita_tipo" class="form-control col-md-7 col-xs-12 ">
                                    <option value="litri" <?php if($rifornimenti->quantita_tipo == 'litri') echo "selected"; ?> >litri</option>
                                    <option value="kg" <?php if($rifornimenti->quantita_tipo == 'kg') echo "selected"; ?> >kg</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prezzo al litro</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{$rifornimenti->prezzo_unitario }}" type="number" step="any" id="prezzo_unitario" name="prezzo_unitario" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Imponibile</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{$rifornimenti->imponibile }}" type="number" id="imponibile" step="any"  name="imponibile" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Iva</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{$rifornimenti->iva }}" type="number" id="iva" name="iva" step="any" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Totale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{$rifornimenti->totale }}" type="number" step="any" id="totale" name="totale" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fattura</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_fattura" name="id_fattura" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\FatturaAcquisto::all() as $fatt){

                                    $fatturaAzienda = "";
                                    if($fatt->id_azienda){
                                        $fatturaAzienda = \App\Models\Azienda::find($fatt->id_azienda);
                                        $fatturaAzienda = $fatturaAzienda->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $fatt->id }}" <?php if($rifornimenti->id_fattura == $fatt->id) echo "selected"; ?>> {{ $fatt->numero }} - {{ $fatturaAzienda }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>
        $(document).ready(function() {

            $("#id_veicolo").select2({
                placeholder: "Seleziona veicolo",
                allowClear: true
            });

            $("#id_rimorchio").select2({
                placeholder: "Seleziona rimorchio",
                allowClear: true
            });

            $("#id_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });


            //DATA


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });



        });
    </script>





    <!-- /Select2 -->


@endsection
