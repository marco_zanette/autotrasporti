
@extends('layouts.main')



@section('content_page')


    <h1> Rifornimenti</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista dei rifornimenti</h2>
                <ul class="add_all nav navbar-right panel_toolbox">
                    <li>
                        <a href="<?php echo route('rifornimenti::add')?>" class="btn btn-primary">Aggiungi un nuovo rifornimento<i class="fa fa-plus"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Data</th>
                        <th>Veicolo</th>
                        <th>Chilometri</th>
                        <th>Prodotto</th>
                        <th>Quantità</th>
                        <th>Prezzo al litro</th>
                        <th>Imponibile</th>
                        <th>Iva</th>
                        <th>Totale</th>
                        <th width="100"></th>
                        <th width="100"></th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($rifornimenti as $rif){

                        $veicolo_str = "";
                        if($rif->id_veicolo != 0){
                            $veicoloOBJ = \App\Models\Veicolo::find($rif->id_veicolo);
                            if($veicoloOBJ){
                                $veicolo_str = $veicoloOBJ->targa;
                            }
                        }

                    ?>
                    <tr style="background-color: @if($rif->id_fattura == 0) #F59580 @else #C0F3C3 @endif">
                        <td>
                            <?php

                            $dataOBJ = new \Carbon\Carbon($rif->data);

                            echo $dataOBJ->format('d/m/Y');
                            ?>

                            <div class="modal fade" id="ModalConfirmDelete_{{$rif->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" style="color: #f00; font-size: 20px;">Attenzione</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                Sei sicuro di voler cancellare l'elemento selezionato?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                                            <a href="<?php echo route('rifornimenti::delete',['id' => $rif->id ]) ?>" class="btn btn-danger">Si, cancella</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td><?php echo $veicolo_str; ?></td>
                        <td><?php echo $rif->km; ?></td>
                        <td><?php echo $rif->prodotto; ?></td>
                        <td><?php echo $rif->quantita; ?></td>
                        <td><?php echo $rif->prezzo_unitario; ?></td>
                        <td><?php echo $rif->imponibile; ?></td>
                        <td><?php echo $rif->iva; ?></td>
                        <td><?php echo $rif->totale; ?></td>
                        <td><a href="<?php echo route('rifornimenti::edit',['id' => $rif->id])?>" class="btn btn-dark">Modifica</a></td>
                        <td>

                            <button type="button" class="btn btn-danger" data-target="#ModalConfirmDelete_{{$rif->id}}" data-toggle="modal" >Elimina</button>

                        </td>
                    </tr>

                    <?php
                    }
                    ?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>






@endsection





@section('footer_script')
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable();


        });
    </script>
    <!-- /Datatables -->


@endsection