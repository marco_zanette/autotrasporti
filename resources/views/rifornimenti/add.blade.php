
@extends('layouts.main')



@section('content_page')


    <h1>Nuovo rifornimento</h1>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Data</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="">
                                    <input type="text" class="form-control has-feedback-left" id="data" name="data" placeholder="" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" ></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_veicolo" name="id_veicolo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Veicolo::all() as $veicolo){
                                    ?>
                                    <option value="{{ $veicolo->id }}">{{ $veicolo->nome }} {{ $veicolo->targa }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!--
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rimorchio</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_rimorchio" name="id_rimorchio" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Rimorchio::all() as $rimorchio){
                                    ?>
                                    <option value="{{ $rimorchio->id }}">{{ $rimorchio->nome }} {{ $rimorchio->targa }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        -->

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Luogo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_luogo" name="id_luogo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Luogo::all() as $luogo){

                                        $luogo_azienda = "";

                                        if($luogo->id_azienda){
                                            $aziendaLuogo = \App\Models\Azienda::find($luogo->id_azienda);
                                            $luogo_azienda = $aziendaLuogo->ragsoc.",";
                                        }

                                    ?>
                                    <option value="{{ $luogo->id }}">{{$luogo_azienda}} {{ $luogo->nome }} - {{ $luogo->comune }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Km</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" id="km" name="km" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prodotto</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="prodotto" name="prodotto" class="form-control" >
                                    <option value="diesel">Diesel</option>
                                    <option value="adblue">Adblue</option>
                                    <option value="olio motore">Olio motore</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quantità</label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <input type="number" id="quantita" name="quantita" step="any" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <select name="quantita_tipo" class="form-control col-md-7 col-xs-12 ">
                                    <option value="litri">litri</option>
                                    <option value="kg">kg</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prezzo al litro</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" step="any" id="prezzo_unitario" name="prezzo_unitario" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Imponibile</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" step="any" id="imponibile" name="imponibile" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Iva</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" step="any" id="iva" name="iva" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Totale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" step="any" id="totale" name="totale" value="0" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fattura</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_fattura" name="id_fattura" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\FatturaAcquisto::all() as $fatt){

                                    $fatturaAzienda = "";
                                    if($fatt->id_azienda){
                                        $fatturaAzienda = \App\Models\Azienda::find($fatt->id_azienda);
                                        $fatturaAzienda = $fatturaAzienda->ragsoc;
                                    }

                                    ?>
                                    <option value="{{ $fatt->id }}" > {{ $fatt->numero }} - {{ $fatturaAzienda }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Aggiungi</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>
        $(document).ready(function() {

            $("#id_veicolo").select2({
                placeholder: "Seleziona veicolo",
                allowClear: true
            });

            $("#id_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });

            $("#id_azienda").select2({
                placeholder: "Seleziona fornitore",
                allowClear: true
            });

            //DATA


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });



        });
    </script>





    <!-- /Select2 -->


@endsection


