<?php
/**
 * Date: 12/03/17
 * Time: 16:36
 */


$azienda = \App\Models\Azienda::find($id_azienda);


$array_viaggi = json_decode($viaggi);


$dataOK = new \Carbon\Carbon($data);


?>
<html>

<body>

<style type="text/css">
    @page {
        header: page-header;
        footer: page-footer;
        margin-top: 100mm;
    }
</style>

<?php
  $nazAZ = \App\Models\Nazione::where('iso',$azienda->nazione)->first();
?>

<htmlpageheader name="page-header">
    <img src="/img/logo-fattura.jpg" style="height: 50mm; margin-right: 10mm;">

    <div style="margin-left: 100mm; margin-top: -10mm; font-size: 9pt;">
        <strong>Spettabile<br />
        {{ $azienda->ragsoc }} </strong><br />
        {{ $azienda->indirizzo }}
        <br />
        {{ $azienda->cap }} - {{ $azienda->comune }} {{ $azienda->provincia }}
        <br />
        {{ $nazAZ->nome }}
        <br />
        Partita IVA {{ $azienda->piva }}
        Codice Fiscale {{ $azienda->codfisc }}
    </div>
    <div>
        <table style="font-size: 9pt;">
            <tr>
                <td><strong>DATA</strong></td>
                <td>{{ $dataOK->format("d/m/Y") }}</td>
            </tr>
            <tr>
                <td><strong>PAGINA</strong></td>
                <td>{PAGENO} di  {nbpg} </td>
            </tr>
            <tr>
                <td><strong>FATTURA N.</strong></td>
                <td>{{ $numero }}</td>
            </tr>
            <tr>
                <td><strong>COND. PAGAM.</strong></td>
                <td>{{ $azienda->pagamento_cliente }}</td>
            </tr>
            <tr>
                <td><strong>VS.BANCA</strong></td>
                <td>{{ $azienda->banca }}</td>
            </tr>
            <tr>
                <td><strong>VS.IBAN</strong></td>
                <td>{{ $azienda->iban }}</td>
            </tr>
        </table>
    </div>
</htmlpageheader>

<htmlpagefooter name="page-footer">
    <div style="text-align: right; font-size: 9pt; line-height: 20pt;">pag. {PAGENO}</div>
</htmlpagefooter>

<style type="text/css">
    .fattura{
        border-collapse: collapse;
    }

    .fattura th{
        background-color: #c0c0c0;
        border: solid 0.2mm #c0c0c0;
        font-size: 9pt;
    }

    .fattura td{
        font-size: 9pt;
    }


</style>

<table class="fattura" width="100%" style="text-align: left;" >
    <thead>
    <tr>
        <th style="width: 25mm;">Quantità</th>
        <th style="width: 20mm;">U.M.</th>
        <th>Descrizione</th>
        <th style="width: 40mm;">Importo</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($array_viaggi as $viaggio){

        if(!isset($viaggio->id)) continue;

        $viaggioOBJ = \App\Models\Viaggio::find($viaggio->id);

        if(!$viaggioOBJ){
            continue;
        }


        $descrizioneV = "";


        $descrizioneV .= "Rif. Vs. ".$viaggioOBJ->riferimento_tipo." N° ".$viaggioOBJ->riferimento_n;
        if($viaggioOBJ->riferimento_azienda != 0){
            $azRifOjb = \App\Models\Azienda::find($viaggioOBJ->riferimento_azienda);
            if($azRifOjb){
                $descrizioneV .= " ".$azRifOjb->ragsoc;
            }
        }
        $descrizioneV .= "<br />";


        foreach ($viaggioOBJ->scarichi as $scaricoOBJ){
            foreach ($scaricoOBJ->carichi as $caricoID){
                $caricoOBJ = \App\Models\Carico::find($caricoID);


                $descrizioneV .= "Trasporto da ".$caricoOBJ->luogo->nazione."-".$caricoOBJ->luogo->cap."-".$caricoOBJ->luogo->comune." (".$caricoOBJ->luogo->provincia.")";
                $descrizioneV .= " a ".$scaricoOBJ->luogo->nazione."-".$scaricoOBJ->luogo->cap."-".$scaricoOBJ->luogo->comune." (".$caricoOBJ->luogo->provincia.")<br />";
            }
        }


        /*
        if($viaggioOBJ->riferimento_tipo != ""){
            $descrizioneV .= "Riferimento ".$viaggioOBJ->riferimento_tipo. " n.".$viaggioOBJ->riferimento_n;
        }
        if($viaggioOBJ->riferimentoAzienda){
            $descrizioneV .= " ".$viaggioOBJ->riferimentoAzienda->ragsoc;
        }
        */

        foreach($viaggioOBJ->riferimenti as $rifOBJ){
            $descrizioneV .= "rif. ".$rifOBJ->tipo. " N°".$rifOBJ->numero;
            if($rifOBJ->azienda != 0){
                $aziendaRif = \App\Models\Azienda::find($rifOBJ->azienda);
                $descrizioneV .= " ".$aziendaRif->ragsoc;
            }
            $descrizioneV .= "<br />";
        }






        ?>
        <tr>
            <td style="text-align: center; border: solid 0.2mm #000;">1</td>
            <td style="text-align: center; border: solid 0.2mm #000;">NR.</td>
            <td style=" border: solid 0.2mm #000;"><?php echo $descrizioneV ?></td>
            <td style="text-align: right; border: solid 0.2mm #000;">€ <?php echo number_format($viaggio->prezzo,2,",",""); ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">Totale imponibile</td>
            <td style="text-align: right; font-weight: bold;background-color: #c0c0c0;">€ <?php echo number_format($imponibile,2,",",""); ?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">IVA {{ $iva_applicata }}%</td>
            <td style="text-align: right; font-weight: bold;background-color: #c0c0c0;">€ <?php echo number_format($iva,2,",",""); ?></td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right; font-weight: bold;">Totale</td>
            <td style="text-align: right; font-weight: bold;background-color: #c0c0c0;">€ <?php echo number_format($totale,2,",",""); ?></td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 8pt;">
                <br />
                Ai sensi e per gli effetti di cui all'art.83bis comme 6 della legge 133/08 e successive modificazioni ed integrazioni,
                si evidenzia che la quota parte del corrispettivo corrispondente al costo del carburante per l'esecuzione delle
                prestazioni sopra indicate, corrisponde al 30% del corrispettivo stesso.
            </td>
        </tr>
    </tfoot>

</table>

<br />
<span style="font-size: 9pt;">
    {{ $descrizione }}
</span>
<br /><br />

<div style="font-size: 9pt;">
    <strong>
        In caso di mancata Ri.Ba., vogliate effettuare bonifico bancario a:
    </strong>
    <br />
    TREVISAN MAURO DITTA<br />
    BANCA MONTE DEI PASCHI DI SIENA - Agenzia di Cessalto (TV)<br />
    IT 61 A 01030 61590 000000513145
</div>

</body>

</html>