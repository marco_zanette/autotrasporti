<?php
/**
 * Date: 12/03/17
 * Time: 16:36
 */


$azienda = \App\Models\Azienda::find($id_azienda);


$dataOK = new \Carbon\Carbon($data);


$carico_dataOK = new \Carbon\Carbon($carico_data);

$scarico_dataOK = new \Carbon\Carbon($scarico_data);



$luogoCarico = \App\Models\Luogo::find($id_luogo_carico);
$azienaCaricoStr = "";
if($luogoCarico and $luogoCarico->id_azienda != 0){
    $azienaCarico = \App\Models\Azienda::find($luogoCarico->id_azienda);
    $azienaCaricoStr = $azienaCarico->ragsoc;
}

$luogoScarico = \App\Models\Luogo::find($id_luogo_scarico);
$azienaScaricoStr = "";
if($luogoScarico and $luogoScarico->id_azienda != 0){
    $azienaScarico = \App\Models\Azienda::find($luogoScarico->id_azienda);
    $azienaScaricoStr = $azienaScarico->ragsoc;
}


?>
<html>

<body>

<style type="text/css">
    @page {
        header: page-header;
        footer: page-footer;
        margin-top: 60mm;
    }

    table.spett {
        border: solid 0.2mm #000;
        border-collapse: collapse;
        width: 100%;
    }

    table.spett td{
        border: solid 0.2mm #000;
        font-size: 9pt;
        padding: 1mm;
    }
</style>


<htmlpageheader name="page-header">

    <img src="/img/logo-fattura.jpg" style="height: 50mm; margin-right: 10mm;">

    <div style="margin-left: 100mm; margin-top: -43mm;">

        <table class="spett">
            <tr>
                <td style="width: 45mm; font-weight: bold;">
                    ORDINE DI TRASPORTO<br />
                    Forwarding Agreement
                </td>
                <td>
                    Nr. <strong>{{ $numero }}</strong><br />
                    del <strong>{{ $dataOK->format('d/m/Y') }}</strong>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <strong>Spettabile<br />
                    {{ $azienda->ragsoc }} </strong><br />
                    {{ $azienda->indirizzo }} {{ $azienda->cap }} - {{ $azienda->comune }} {{ $azienda->provincia }}
                </td>
            </tr>
        </table>

    </div>
</htmlpageheader>



<htmlpagefooter name="page-footer">
    <div style="text-align: right; font-size: 9pt; line-height: 20pt;">pag. {PAGENO}</div>
</htmlpagefooter>



<style type="text/css">
    .fattura{
        border-collapse: collapse;
        font-size: 11pt;
    }

</style>



<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 45mm;">
            1° Luogo di carico <br />
            1° Loading place
        </td>
        <td style="font-weight: bold;">
            {{ $azienaCaricoStr }} - {{$luogoCarico->nome}}<br/>
            {{$luogoCarico->indirizzo}} - {{$luogoCarico->cap}} - {{$luogoCarico->comune}} ({{$luogoCarico->provincia}})
        </td>

    </tr>
</table>

<br />


<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 45mm;">
            1° Luogo di scarico <br />
            1° Unloading place
        </td>
        <td style="font-weight: bold;">
            {{ $azienaScaricoStr }} - {{$luogoScarico->nome}}<br/>
            {{$luogoScarico->indirizzo}} - {{$luogoScarico->cap}} - {{$luogoScarico->comune}} ({{$luogoScarico->provincia}})
        </td>

    </tr>
</table>


<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 45mm;">
            Tipo di merce <br />
            Goods description
        </td>
        <td style="font-weight: bold;">
            {{ $merce }}
        </td>
    </tr>
</table>



<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 20mm;">
            Carico <br />
            Load
        </td>
        <td style="font-weight: bold;">
            {{ $carico }}
        </td>
        <td style="width: 25mm; border-left: solid 0.2mm #000;">
            Metri lineari <br />
            Load MTL
        </td>
        <td style="font-weight: bold;">
            {{ $ml }}
        </td>

        <td style="width: 20mm; border-left: solid 0.2mm #000;">
            Mc
        </td>
        <td style="font-weight: bold;">
            {{ $mc }}
        </td>

        <td style="width: 20mm; border-left: solid 0.2mm #000;">
            Kg
        </td>
        <td style="font-weight: bold;">
            {{ $kg }}
        </td>
    </tr>
</table>



<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 45mm; border: 0.2mm solid #000;">
            Tipo veicolo <br />
            Vehicle description
        </td>
        <td style="font-weight: bold; border: 0.2mm solid #000;">
            {{ $veicolo }}
        </td>
        <td style="width: 70mm; border: 0.2mm solid #000; padding: 0;">
            <table style="border-collapse: collapse; width: 70mm;">
                <tr>
                    <td style=" border: 0.8mm solid #000; width: 30mm; font-size: 10pt;">
                        Targa motrice<br />
                        Tractor number plate
                    </td>
                    <td style=" border: 0.8mm solid #000; background-color: #c0c0c0;">

                    </td>
                </tr>
                <tr>
                    <td style=" border: 0.8mm solid #000; font-size: 10pt;">
                        Targa rimorchio<br />
                        Trailer number plate
                    </td>
                    <td style=" border: 0.8mm solid #000; background-color: #c0c0c0;">

                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>


<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 55mm;">
            Carico tassativo il: <br />
            Peremptory loading date on:
        </td>
        <td style="font-weight: bold;">
            {{ $carico_dataOK->format('d/m/Y') }}
        </td>
        <td style="width: 35mm;">
            Ore: <br />
            Loading hours:
        </td>
        <td style="font-weight: bold;">
            {{ $carico_ora }}
        </td>
    </tr>
</table>


<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 55mm;">
            Scarico tassativo il: <br />
            Peremptory delivery date on:
        </td>
        <td style="font-weight: bold;">
            {{ $scarico_dataOK->format('d/m/Y') }}
        </td>
        <td style="width: 35mm;">
            Ore: <br />
            Unloading hours:
        </td>
        <td style="font-weight: bold;">
            {{ $scarico_ora }}
        </td>
    </tr>
</table>


<br />

<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 55mm;">
            Pagamento <br />
            Term of payment
        </td>
        <td style="font-weight: bold;">
            {{ $pagamento }}
        </td>
    </tr>
</table>




<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 55mm;">
            Prezzo offerto al vettore <br />
            Freight rate agreed
        </td>
        <td style="font-weight: bold; width: 35mm;">
            € {{ number_format($prezzo,2,",","") }}
        </td>
        <td style="font-weight: bold;">
            ATTENZIONE: l'importo del trasporto accordato, deve restare SEGRETO e non deve essere per nessun motivo mostrato né sul luogo di carico né su quello di scarico <br />
            CAUTION: the amount of the granted transport, must remain SECRET and should not be shown for no reason either at the place of loading or unloading
        </td>
    </tr>
</table>

<br />

<div style="border-bottom: 0.2 solid #000; font-size: 9pt;">
    IMPORTANTE: IL PAGAMENTO DELLA FATTURA E' VINCOLATO AL RICEVIMENTO DEL DDT O CMR ORIGINALE CONTROFIRMATO<br />
    IMPORTANT: PAYMENT OF INVOICE IT'S BOUND IN RECEIPT OF DDT OR CMR ORIGINAL COUNTERSIGNED
</div>



<br />



<table class="fattura" width="100%" style="text-align: left; border: 0.2mm solid #000;" >
    <tr>
        <td style="width: 30mm;">
            Data e luogo <br />
            Date and place
        </td>
        <td style="background-color: #c0c0c0;">

        </td>
        <td style="width: 30mm;">
            Firma e timbro <br />
            Signature and stamp
        </td>
        <td style="background-color: #c0c0c0;">

        </td>
    </tr>
</table>

<div style="font-size: 9pt; font-weight: bold;">
    Completare i riquadri in grigio e rispedire al mittente.<br />
    Complete the boxes in gray and send back.
</div>



</body>

</html>