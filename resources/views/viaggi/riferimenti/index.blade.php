
<br />
<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap">

    <tr>
        <td>Numero</td>
        <td>Data</td>
        <td>Tipo</td>
        <td>Azienda</td>
        <!--<td width="80">File</td>-->
        <td width="80"></td>
        <td width="80"></td>
    </tr>

    <?php
    foreach(\App\Models\Riferimentoviaggio::where("id_viaggio",$viaggio->id)->get() as $riferimento){

        $azienda = "";
        if($riferimento->azienda != 0){

            $aziendaRif = \App\Models\Azienda::find($riferimento->azienda);
            if($aziendaRif){
                $azienda = $aziendaRif->ragsoc;
            }

        }

        $dataOBJ = new Carbon\Carbon($riferimento->data);

    ?>
    <tr>
        <td> {{ $riferimento->numero }}</td>
        <td> {{ $dataOBJ->format('d/m/Y') }}</td>
        <td> {{ $riferimento->tipo }} </td>
        <td> {{ $azienda }}</td>

        <!--
        <td>
            <a target="_blank" href="<?php echo env('APP_URL')."/".$riferimento->documento; ?>" class="btn btn-info btn-xs">Donwload</a>
        </td>
        -->
        <td>
            <a href="<?php echo route("viaggi::riferimento-edit",['id'=>$riferimento->id]); ?>" class="btn btn-dark btn-xs" style="margin: 0;">Modifica</a>
        </td>
        <td>
            <a href="<?php echo route("viaggi::riferimento-delete",['id'=>$riferimento->id]); ?>" class="btn btn-danger btn-xs" style="margin: 0;">Elimina</a>
        </td>
    </tr>
    <?php
    }
    ?>

</table>



