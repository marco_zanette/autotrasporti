

<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">


    <div class="row">

        <div class="col-md-3 col-sm-3 col-xs-12">
            <label for="numero">Numero</label>
            <div class="">
                <input type="text" id="numero" name="numero" class="form-control col-md-7 col-xs-12">
            </div>
        </div>


        <div class="col-md-3 col-sm-3 col-xs-12">
            <label for="numero">Data</label>
            <div class="">
                <input type="text" id="data" name="data" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-sm-3 col-xs-12">
            <label for="metri_lineari">Tipo</label>
            <div class="">
                <select name="tipo" class="form-control">
                    <option value="">Seleziona tipo</option>
                    <?php
                    foreach(\App\Models\Tiporiferimento::all() as $tipo){
                    ?>
                    <option value="{{ $tipo->nome }}" >{{ $tipo->nome }}</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="form-group col-sm-3 col-xs-12">
            <label for="metri_lineari">Azienda</label>
            <div class="">
                <select id="azienda" name="azienda" class="form-control" tabindex="-1">
                    <option value="">seleziona azienda</option>
                    <?php

                    foreach(App\Models\Azienda::orderBy('ragsoc')->get() as $az){
                    ?>
                    <option value="{{ $az->id }}" >{{ $az->ragsoc }}</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="form-group col-md-8 col-xs-12">

            <label>File</label>

            <div class="input-group">
                               <span class="input-group-btn">
                                 <a id="lfm2" data-input="thumbnail2" data-preview="holder" class="btn btn-primary">
                                   <i class="fa fa-picture-o"></i> Choose
                                 </a>
                               </span>
                <input id="thumbnail2" class="form-control" type="text" name="filepath" value="">
            </div>

            <input type="button" class="btn btn-default" onclick="$('#thumbnail2').val('');" value="Elimina file scelto" />


        </div>

    </div>





    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Inserisci riferimento</button>
        </div>
    </div>

    {{ csrf_field() }}


    <input type="hidden" name="add_riferimento" value="1">

</form>



<script>



</script>