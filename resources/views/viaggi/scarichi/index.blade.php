
<br />
<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap">

    <tr>
        <td>Data</td>
        <td>Luogo</td>
        <td>Km</td>
        <td>Arrivo</td>
        <td>da - a</td>
        <td width="80"></td>
        <td width="80"></td>
    </tr>

    <?php
    foreach(\App\Models\Scarico::where("id_viaggio",$viaggio->id)->get() as $carico){

        $luogo_str = "";
        if($carico->id_luogo != 0){
            $luogoOBJ = \App\Models\Luogo::find($carico->id_luogo);
            $luogo_str = $luogoOBJ->nome;

            if($luogoOBJ->id_azienda != 0){
                $aziendaOBJ = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                $luogo_str = $aziendaOBJ->ragsoc." ".$luogo_str;
            }
        }

        $dataOBJ = new Carbon\Carbon($carico->data);

    ?>
    <tr>
        <td> {{ $dataOBJ->format('d/m/Y') }}</td>
        <td> {{ $luogo_str }}</td>
        <td> {{ $carico->km }}</td>
        <td> {{ formatOra($carico->ora_arrivo) }}</td>
        <td> {{ formatOra($carico->ora_inizio_carico) }} - {{ formatOra($carico->ora_fine_carico) }}</td>
        <td>
            <a href="<?php echo route("viaggi::scarico-edit",['id'=>$carico->id]); ?>" class="btn btn-dark btn-xs" style="margin: 0;">Modifica</a>
        </td>
        <td>
            <a href="<?php echo route("viaggi::scarico-delete",['id'=>$carico->id]); ?>" class="btn btn-danger btn-xs" style="margin: 0;">Elimina</a>
        </td>
    </tr>
    <?php
    }
    ?>

</table>



