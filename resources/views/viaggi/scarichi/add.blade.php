

<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">



    <div class="row">

        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="data">Data</label>
            <div class="">
                <input type="text" class="form-control has-feedback-left" id="data_scarico" name="data" placeholder="" aria-describedby="inputSuccess2Status">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" style="margin-top: 30px;"></span>
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="id_luogo">Luogo</label>
            <div class="">
                <select id="id_luogo_scarico" name="id_luogo" class="form-control" tabindex="-1">
                    <option></option>
                    <?php
                    $lista_luoghi = array();
                    foreach(\App\Models\Luogo::orderBy('nome','asc')->get() as $luogo){

                        $rag_soc = "";
                        if($luogo->id_azienda != 0){
                            $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                            $rag_soc = $aziendaOBJ->ragsoc;
                        }

                        $lista_luoghi[] = array(
                            'id' => $luogo->id,
                            'nome' => $rag_soc." - ".$luogo->nome
                        );
                    }
                    $lista_luoghi = collect($lista_luoghi);

                    foreach($lista_luoghi->sortBy('nome') as $luogo){
                    ?>
                    <option value="{{ $luogo['id'] }}" >{{ $luogo['nome'] }}</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>



        <div class="form-group col-md-3 col-sm-3 col-xs-12">

            <label class="" for="km">Km</label>
            <div class="">
                <input value="0" type="number" class="form-control" name="km" id="km" />
            </div>
        </div>

    </div>


    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Carichi</label>
        <div class="col-md-6 col-sm-6 col-xs-12">

            <select id="carichi" name="carichi[]" class="form-control" tabindex="-1" multiple="multiple">
                <option></option>
                <?php
                foreach(\App\Models\Carico::where("id_viaggio",$viaggio->id)->get() as $car){

                    $luogoSTR = "";
                    if($car->id_luogo){
                        $luogoOBJ = \App\Models\Luogo::find($car->id_luogo);
                        $luogoSTR = $luogoOBJ->nome;


                        if($luogoOBJ->id_azienda != 0){
                            $aziendaOBJ = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                            $luogoSTR = $aziendaOBJ->ragsoc." ".$luogoSTR;
                        }

                    }

                    $dataOBJ = new Carbon\Carbon($car->data);

                ?>
                <option value="{{ $car->id }}" >{{ $luogoSTR }} ({{ $dataOBJ->format('d/m/Y') }} - {{ formatOra($car->ora_arrivo) }})</option>
                <?php
                }
                ?>
            </select>

        </div>
    </div>







    <div class="row">

        <div class="col-md-2 col-sm-2 col-md-offset-3 col-xs-12">
            <label for="ora_arrivo">Arrivo</label>
            <div class="">
                <input value="00:00" type="text" id="ora_arrivo" name="ora_arrivo" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-2 col-sm-3 col-xs-12">
            <label for="ora_inizio_carico">Inizio scarico</label>
            <div class="">
                <input value="00:00" type="text" id="ora_inizio_carico" name="ora_inizio_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-of">
            <label for="ora_fine_carico">Fine scarico</label>
            <div class="">
                <input value="00:00" type="text" id="ora_fine_carico" name="ora_fine_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>


        <div class="form-group col-md-4 col-sm-4 col-xs-12 ">

            <label class="" for="note">Note</label>
            <div class="">
                <textarea class="resizable_textarea form-control" name="note" id="note"></textarea>
            </div>

        </div>

    </div>

    

    <!--

    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo_merce">Tipo merce</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control" name="tipo_merce" id="tipo_merce" />
        </div>
    </div>





    <div class="row">

        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="quantita">Quantità</label>
            <div class="">
                <input type="text" id="quantita" name="quantita" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="metri_lineari">Metri lineari</label>
            <div class="">
                <input type="text" id="metri_lineari" name="metri_lineari" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

    </div>



    <div class="row">

        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="volume">Volume</label>
            <div class="">
                <input type="text" id="volume" name="volume" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="peso">Peso</label>
            <div class="">
                <input type="text" id="peso" name="peso" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

    </div>
    -->



    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>

    {{ csrf_field() }}


    <input type="hidden" name="add_scarico" value="1">

</form>

