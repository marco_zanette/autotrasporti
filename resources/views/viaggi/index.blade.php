
@extends('layouts.main')



@section('content_page')


    <h1> Viaggi</h1>


    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista dei viaggi</h2>
                <ul class="add_all nav navbar-right panel_toolbox">
                    <li>
                        <a href="<?php echo route('viaggi::add')?>" class="btn btn-primary">Aggiungi un nuovo viaggio<i class="fa fa-plus"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Giorno</th>
                        <th>Luogo carico</th>
                        <th>KM</th>
                        <th>Ora arrivo</th>
                        <th>Documento</th>
                        <th>Luogo scarico</th>
                        <th>KM</th>
                        <th>Ora arrivo</th>
                        <th>Fatturare a</th>
                        <th width="100"></th>
                        <th width="100"></th>
                        <th width="100"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach ($viaggi as $viag){


                        // controllo se già in qualche fattura
                        $isFattura =  false;
                        $qCheck = "SELECT * FROM fatture WHERE viaggi LIKE '%\"id\":".$viag->id.",%' ";
                        $risCheck = \DB::select($qCheck);
                        if(count($risCheck) > 0){
                            $isFattura =  true;
                        }


                        if($isFattura == false){
                            if($viag->from_ordine == 1){
                                $color = "#D4F6FB";
                            } else {
                                $color = "#F59580";
                            }
                        } else {
                            $color = "#C0F3C3";
                        }


                        $veicoloOBJ = \App\Models\Veicolo::find($viag->id_veicolo);

                        $rimorchioOBJ = \App\Models\Rimorchio::find($viag->id_rimorchio);

                        $rag_soc = "";
                        if($viag->id_azienda != 0){
                            $aziendaOBJ = \App\Models\Azienda::find($viag->id_azienda);
                            $rag_soc = $aziendaOBJ->ragsoc;
                        }


                        $giorno = "";
                        $luogo = "";
                        $km = "";
                        $ora = "";
                        //$caricoOBJ = \App\Models\Carico::where("id_viaggio",$viag->id)->first();
                        $lista_caricoOBJ = \App\Models\Carico::where("id_viaggio",$viag->id)->get();

                        foreach ($lista_caricoOBJ as $caricoOBJ){

                            if($caricoOBJ){

                                $dataOBJ = new \Carbon\Carbon($caricoOBJ->data);

                                $giorno .= $dataOBJ->format('d/m/Y')."<br />";
                                $km .= $caricoOBJ->km."<br />";
                                $luogo .= "";
                                $ora .= formatOra($caricoOBJ->ora_arrivo)."<br />";

                                if($caricoOBJ->id_luogo != 0){

                                    $luogoOBJ = \App\Models\Luogo::find($caricoOBJ->id_luogo);


                                    if($luogoOBJ->id_azienda){
                                        $aziendaLuogo = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                                        $luogo .= $aziendaLuogo->ragsoc.", ".$luogoOBJ->nome."<br />";
                                    } else {
                                        $luogo .= $luogoOBJ->nome."<br />";
                                    }

                                }
                            }

                        }




                        // scarico
                        $luogo_s = "";
                        $km_s = "";
                        $ora_s = "";
                        $scaricoOBJ = \App\Models\Scarico::where("id_viaggio",$viag->id)->first();
                        if($scaricoOBJ){

                            $km_s .= $scaricoOBJ->km."<br />";
                            $luogo_s = "";
                            $ora_s .= formatOra($scaricoOBJ->ora_arrivo)."<br />";

                            if($scaricoOBJ->id_luogo != 0){

                                $luogoOBJ = \App\Models\Luogo::find($scaricoOBJ->id_luogo);


                                if($luogoOBJ->id_azienda){
                                    $aziendaLuogo = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                                    $luogo_s .= $aziendaLuogo->ragsoc.", ".$luogoOBJ->nome." <br />";
                                } else {
                                    $luogo_s .= $luogoOBJ->nome."<br />";
                                }

                            }
                        }


                        $riferimentoSTR = "";
                        foreach ($viag->riferimenti as $rifOBJ){

                            if($rifOBJ->documento != ''){
                                $riferimentoSTR .= "<a href='".env('APP_URL')."/".$rifOBJ->documento."' target='_blank'> <i style='font-size: 20px;' class='fa fa-file-pdf-o'></i> ";
                            }

                            $riferimentoSTR .= $rifOBJ->tipo." n. ".$rifOBJ->numero;

                            if($rifOBJ->azienda != 0){
                                $aziendaRiferimento = \App\Models\Azienda::find($rifOBJ->azienda);
                                $riferimentoSTR .= " ".$aziendaRiferimento->ragsoc;
                            }

                            if($rifOBJ->documento != ''){
                                $riferimentoSTR .= "</a>";
                            }
                            $riferimentoSTR .= "<br />";
                        }

                    ?>
                    <tr style="background-color: {{ $color }};">
                        <td>
                            <?php echo $viag->numero; ?>
                        </td>
                        <td><?php echo $giorno ?></td>
                        <td>
                            <?php echo $luogo ?>
                            <div class="modal fade" id="ModalConfirmDelete_{{$viag->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" style="color: #f00; font-size: 20px;">Attenzione</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                Sei sicuro di voler cancellare l'elemento selezionato?
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                                            <a href="<?php echo route('viaggi::delete',['id' => $viag->id ]) ?>" class="btn btn-danger">Si, cancella</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td><?php echo  $km ?></td>
                        <td><?php echo  $ora ?></td>
                        <td>
                            <?php
                            echo $riferimentoSTR;
                            ?>
                        </td>

                        <td>
                            <?php echo  $luogo_s ?>
                        </td>
                        <td><?php echo  $km_s ?></td>
                        <td><?php echo  $ora_s ?></td>


                        <td><?php echo $rag_soc; ?></td>
                        <td>
                            <a href="<?php echo route('viaggi::duplicate',['id' => $viag->id])?>" class="btn btn-dark">Duplica</a>
                        </td>
                        <td>
                            <a href="<?php echo route('viaggi::edit',['id' => $viag->id])?>" class="btn btn-dark">Modifica</a>
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" data-target="#ModalConfirmDelete_{{$viag->id}}" data-toggle="modal" >Elimina</button>
                        </td>
                    </tr>

                    <?php

                    }
                    ?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>






@endsection





@section('footer_script')
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable( {
                "order": [[ 0, 'desc' ]]
            } );


        });
    </script>
    <!-- /Datatables -->


@endsection