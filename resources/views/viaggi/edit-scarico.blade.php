
@extends('layouts.main')



@section('content_page')


    <h1>Modifica scarico</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <ul class="add_all nav navbar-right panel_toolbox">
                        <li>
                            <a href="<?php echo route("viaggi::edit",['id'=>$carico->id_viaggio]); ?>" class="btn btn-primary"> <i class="fa fa-chevron-left"></i> Torna al viaggio</a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
                                <label for="data">Data</label>
                                <div class="">
                                    <?php
                                    $data_ok = new \Carbon\Carbon($carico->data);
                                    ?>
                                    <input value="{{ $data_ok->format("d/m/Y") }}" type="text" class="form-control has-feedback-left" id="data" name="data" placeholder="" aria-describedby="inputSuccess2Status">
                                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" style="margin-top: 30px;"></span>
                                </div>
                            </div>


                            <div class="form-group col-md-3 col-sm-3 col-xs-12">
                                <label for="id_luogo">Luogo</label>
                                <div class="">
                                    <select id="id_luogo" name="id_luogo" class="form-control" tabindex="-1">
                                        <option></option>
                                        <?php
                                        $lista_luoghi = array();
                                        foreach(\App\Models\Luogo::orderBy('nome','asc')->get() as $luogo){

                                            $rag_soc = "";
                                            if($luogo->id_azienda != 0){
                                                $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                                                $rag_soc = $aziendaOBJ->ragsoc;
                                            }

                                            $lista_luoghi[] = array(
                                                'id' => $luogo->id,
                                                'nome' => $rag_soc." - ".$luogo->nome
                                            );
                                        }
                                        $lista_luoghi = collect($lista_luoghi);

                                        foreach($lista_luoghi->sortBy('nome') as $luogo){
                                        ?>
                                        <option <?php if($luogo['id'] == $carico->id_luogo) echo "selected"; ?> value="{{ $luogo['id'] }}" >{{ $luogo['nome'] }}</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Carichi</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <select id="carichi" name="carichi[]" class="form-control" tabindex="-1" multiple="multiple">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Carico::where("id_viaggio",$carico->id_viaggio)->get() as $car){

                                    $luogoSTR = "";
                                    if($car->id_luogo){
                                        $luogoOBJ = \App\Models\Luogo::find($car->id_luogo);
                                        $luogoSTR = $luogoOBJ->nome;


                                        if($luogoOBJ->id_azienda != 0){
                                            $aziendaOBJ = \App\Models\Azienda::find($luogoOBJ->id_azienda);
                                            $luogoSTR = $aziendaOBJ->ragsoc." ".$luogoSTR;
                                        }

                                    }
                                    $dataOBJ = new Carbon\Carbon($car->data);

                                    ?>
                                    <option value="{{ $car->id }}" <?php if(in_array($car->id,$carico->carichi)) echo "selected"; ?> >{{ $luogoSTR }} ({{ $dataOBJ->format('d/m/Y') }} - {{ formatOra($car->ora_arrivo) }})</option>
                                    <?php
                                    }
                                    ?>
                                </select>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Km</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input class="form-control" type="number" name="km" id="km" value="{{ $carico->km }}" />
                            </div>
                        </div>





                        <div class="row">

                            <div class="col-md-2 col-sm-2 col-md-offset-3 col-xs-12">
                                <label for="ora_arrivo">Arrivo</label>
                                <div class="">
                                    <input value="{{ $carico->ora_arrivo }}" type="text" id="ora_arrivo" name="ora_arrivo" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group col-md-2 col-sm-3 col-xs-12">
                                <label for="ora_inizio_carico">Inizio carico</label>
                                <div class="">
                                    <input value="{{ $carico->ora_inizio_carico }}" type="text" id="ora_inizio_carico" name="ora_inizio_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-of">
                                <label for="ora_fine_carico">Fine carico</label>
                                <div class="">
                                    <input value="{{ $carico->ora_fine_carico }}" type="text" id="ora_fine_carico" name="ora_fine_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="note">Note</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="resizable_textarea form-control" name="note" id="note">{{ $carico->note }}</textarea>
                            </div>
                        </div>



                        <!--


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipo_merce">Tipo merce</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $carico->tipo_merce }}" class="form-control" name="tipo_merce" id="tipo_merce" />
                            </div>
                        </div>





                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
                                <label for="quantita">Quantità</label>
                                <div class="">
                                    <input value="{{ $carico->quantita }}" type="text" id="quantita" name="quantita" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group col-md-3 col-sm-3 col-xs-12">
                                <label for="metri_lineari">Metri lineari</label>
                                <div class="">
                                    <input value="{{ $carico->metri_lineari }}" type="text" id="metri_lineari" name="metri_lineari" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                        </div>



                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
                                <label for="volume">Volume</label>
                                <div class="">
                                    <input value="{{ $carico->volume }}" type="text" id="volume" name="volume" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group col-md-3 col-sm-3 col-xs-12">
                                <label for="peso">Peso</label>
                                <div class="">
                                    <input value="{{ $carico->peso }}" type="text" id="peso" name="peso" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                        </div>


                        -->




                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>


                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection







@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>

    <!--
    <script src="<?php echo env('APP_URL')?>/tema/production/js/datepicker/daterangepicker.js"></script>
    -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script>
        $(document).ready(function() {


            $("#id_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });


            $("#carichi").select2({
                placeholder: "Seleziona carico di riferimento",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });





        });
    </script>
    <!-- /Select2 -->


@endsection
