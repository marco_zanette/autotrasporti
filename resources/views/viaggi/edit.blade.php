
@extends('layouts.main')



@section('content_page')


    <h1>Modifica viaggio</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_veicolo" name="id_veicolo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Veicolo::all() as $veicolo){
                                        ?>
                                        <option value="{{ $veicolo->id }}" <?php if($viaggio->id_veicolo == $veicolo->id) echo "selected"; ?>>{{ $veicolo->targa }} - {{ $veicolo->nome }}</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rimorchio</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_rimorchio" name="id_rimorchio" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Rimorchio::orderBy("targa")->get() as $rimorchio){
                                    ?>
                                    <option value="{{ $rimorchio->id }}" <?php if($viaggio->id_rimorchio == $rimorchio->id) echo "selected"; ?>>{{ $rimorchio->targa }} - {{ $rimorchio->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fatturare a</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" <?php if($viaggio->id_azienda == $azienda->id) echo "selected"; ?>>{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prezzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $viaggio->prezzo }}" type="number" name="prezzo" class="form-control" />
                            </div>
                        </div>





                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ordine</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">


                                <div class="row">

                                    <div class="col-md-12">

                                        <select name="riferimento_tipo" class="form-control">
                                            <option value="">Seleziona tipo</option>
                                            <option value="ordine" <?php if($viaggio->riferimento_tipo == "ordine") echo "selected"; ?> >ordine</option>
                                            <option value="contratto" <?php if($viaggio->riferimento_tipo == "contratto") echo "selected"; ?> >contratto</option>
                                            <option value="email" <?php if($viaggio->riferimento_tipo == "email") echo "selected"; ?> >email</option>
                                            <option value="telefonata" <?php if($viaggio->riferimento_tipo == "telefonata") echo "selected"; ?> >telefonata</option>
                                            <option value="whatsapp" <?php if($viaggio->riferimento_tipo == "whatsapp") echo "selected"; ?> >whatsapp</option>

                                            <?php
                                            /*
                                            foreach(\App\Models\Tiporiferimento::all() as $tipo){
                                            ?>
                                            <option value="{{ $tipo->nome }}" <?php if($viaggio->riferimento_tipo == $tipo->nome) echo "selected"; ?> >{{ $tipo->nome }}</option>
                                            <?php
                                            }
                                            */
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <br />

                                <div class="row">
                                    <div class="col-md-12">
                                        <input value="{{ $viaggio->riferimento_n }}" type="text" name="riferimento_n" class="form-control" />

                                    </div>
                                </div>

                                <br />
                                <select id="riferimento_azienda" name="riferimento_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" <?php if($viaggio->riferimento_azienda == $azienda->id) echo "selected"; ?> >{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">File</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">


                                <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                    <input id="thumbnail" class="form-control" type="text" name="filepath" value="{{ $viaggio->riferimento_doc }}">
                                </div>

                                <input type="button" class="btn btn-default" onclick="$('#thumbnail').val(''); " value="Elimina file scelto" />

                            </div>
                        </div>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Salva</button>
                            </div>
                        </div>

                        <input type="hidden" name="edit_viaggio" value="1" />

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>




    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title" style="background-color: #E6E9ED; padding: 10px;">
                    <h2>Riferimenti presenti</h2>
                    <div class="clearfix"></div>
                    <div class="x_content">
                        @include('viaggi.riferimenti.index')
                    </div>
                </div>


                <div class="x_title">
                    <h2>Inserisci riferimento</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @include('viaggi.riferimenti.add')
                </div>
            </div>
        </div>
    </div>






    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title" style="background-color: #E6E9ED; padding: 10px;">
                    <h2>Carichi presenti</h2>
                    <div class="clearfix"></div>
                    <div class="x_content">
                        @include('viaggi.carichi.index')
                    </div>
                </div>


                <div class="x_title">
                    <h2>Inserisci carico</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @include('viaggi.carichi.add')
                </div>
            </div>
        </div>
    </div>






    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">

                <div class="x_title" style="background-color: #E6E9ED; padding: 10px;">
                    <h2>Scarichi presenti</h2>
                    <div class="clearfix"></div>
                    <div class="x_content">
                        @include('viaggi.scarichi.index')
                    </div>
                </div>


                <div class="x_title">
                    <h2>Inserisci scarico</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    @include('viaggi.scarichi.add')
                </div>
            </div>
        </div>
    </div>




@endsection







@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>

    <!--
    <script src="<?php echo env('APP_URL')?>/tema/production/js/datepicker/daterangepicker.js"></script>
    -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>


    <script>
        $(document).ready(function() {

            $('#lfm').filemanager('file');

            $('#lfm2').filemanager('file');


            $("#id_veicolo").select2({
                placeholder: "Seleziona veicolo",
                allowClear: true
            });

            $("#id_rimorchio").select2({
                placeholder: "Seleziona rimorchio",
                allowClear: true
            });

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#riferimento_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });




            $("#id_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });
            $("#id_luogo_scarico").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });

            $("#carichi").select2({
                placeholder: "Seleziona carico di riferimento",
                allowClear: true
            });



            $("#azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });





            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#data_carico').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#data_scarico').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });





        });
    </script>
    <!-- /Select2 -->


@endsection
