

<br />
<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">



    <div class="row">

        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="data">Data</label>
            <div class="">
                <input type="text" class="form-control has-feedback-left" id="data_carico" name="data" placeholder="" aria-describedby="inputSuccess2Status">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true" style="margin-top: 30px;"></span>
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="id_luogo">Luogo</label>
            <div class="">
                <select id="id_luogo" name="id_luogo" class="form-control" tabindex="-1">
                    <option></option>
                    <?php
                    $lista_luoghi = array();
                    foreach(\App\Models\Luogo::orderBy('nome','asc')->get() as $luogo){

                        $rag_soc = "";
                        if($luogo->id_azienda != 0){
                            $aziendaOBJ = \App\Models\Azienda::find($luogo->id_azienda);
                            $rag_soc = $aziendaOBJ->ragsoc;
                        }

                        $lista_luoghi[] = array(
                            'id' => $luogo->id,
                            'nome' => $rag_soc." - ".$luogo->nome
                        );
                    }
                    $lista_luoghi = collect($lista_luoghi);


                    foreach($lista_luoghi->sortBy('nome') as $luogo){
                    ?>
                        <option value="{{ $luogo['id'] }}" >{{ $luogo['nome'] }}</option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>


        <div class="form-group col-md-3 col-sm-3 col-xs-12">

            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="km">Km</label>
            <div class="">
                    <input class="form-control" type="number" name="km" id="km" />
            </div>
        </div>

    </div>

    <div class="form-group">

    </div>





    <div class="row">

        <div class="col-md-2 col-sm-2 col-md-offset-3 col-xs-12">
            <label for="ora_arrivo">Arrivo</label>
            <div class="">
                <input type="text" id="ora_arrivo" name="ora_arrivo" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-2 col-sm-3 col-xs-12">
            <label for="ora_inizio_carico">Inizio carico</label>
            <div class="">
                <input type="text" id="ora_inizio_carico" name="ora_inizio_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-of">
            <label for="ora_fine_carico">Fine carico</label>
            <div class="">
                <input type="text" id="ora_fine_carico" name="ora_fine_carico" data-inputmask="'mask' : '99:99'" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

    </div>


    <div class="form-group">
        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label class="" for="note">Note</label>
            <div class="">
                <textarea class="resizable_textarea form-control" name="note" id="note"></textarea>
            </div>
        </div>

        <div class="form-group col-md-6 col-sm-6 col-xs-12">
            <label class="" for="tipo_merce">Tipo merce</label>
            <div class="">
                <input class="form-control" name="tipo_merce" id="tipo_merce" />
            </div>
        </div>
    </div>



    <div class="form-group">

    </div>





    <div class="row">

        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="quantita">Quantità</label>
            <div class="">
                <input type="text" id="quantita" name="quantita" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="metri_lineari">Metri lineari</label>
            <div class="">
                <input type="number" step="any" id="metri_lineari" name="metri_lineari" class="form-control col-md-7 col-xs-12">
            </div>
        </div>


        <div class="col-md-3 col-sm-3 col-md-offset-3 col-xs-12">
            <label for="volume">Volume</label>
            <div class="">
                <input type="text" id="volume" name="volume" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

        <div class="form-group col-md-3 col-sm-3 col-xs-12">
            <label for="peso">Peso</label>
            <div class="">
                <input type="text" id="peso" name="peso" class="form-control col-md-7 col-xs-12">
            </div>
        </div>

    </div>





    <!--
    <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Documento</label>
        <div class="col-md-6 col-sm-6 col-xs-12">


            <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                <input id="thumbnail" class="form-control" type="text" name="filepath" value="">
            </div>
            <img id="holder" style="margin-top:15px;max-height:100px;">

            <input type="button" class="btn btn-default" onclick="$('#thumbnail').val(''); $('#holder').attr('src','');" value="Elimina foto scelta" />

        </div>
    </div>
    -->




    <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>

    {{ csrf_field() }}


    <input type="hidden" name="add_carico" value="1">

</form>

