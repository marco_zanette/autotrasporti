
@extends('layouts.main')



@section('content_page')


    <h1>Nuovo viaggio</h1>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Veicolo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_veicolo" name="id_veicolo" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Veicolo::all() as $veicolo){
                                        ?>
                                        <option value="{{ $veicolo->id }}">{{ $veicolo->targa }} - {{ $veicolo->nome }}</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Rimorchio</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_rimorchio" name="id_rimorchio" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Rimorchio::orderBy("targa")->get() as $rimorchio){
                                    ?>
                                    <option value="{{ $rimorchio->id }}">{{ $rimorchio->targa }} - {{ $rimorchio->nome }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Fatturare a</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}">{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Prezzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" name="prezzo" class="form-control" />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Ordine</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">

                                <div class="row">

                                    <div class="col-md-12">

                                        <select name="riferimento_tipo" class="form-control">
                                            <option value="">Seleziona tipo</option>
                                            <option value="ordine">ordine</option>
                                            <option value="contratto">contratto</option>
                                            <option value="email">email</option>
                                            <option value="telefonata">telefonata</option>
                                            <option value="whatsapp">whatsapp</option>
                                            <?php
                                            /*
                                            foreach(\App\Models\Tiporiferimento::all() as $tipo){
                                            ?>
                                            <option value="{{ $tipo->nome }}"  >{{ $tipo->nome }}</option>
                                            <?php
                                            }
                                            */
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <br />


                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" name="riferimento_n" class="form-control" />

                                    </div>
                                </div>

                                <br />
                                <select id="riferimento_azienda" name="riferimento_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::orderBy('ragsoc', 'asc')->get() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}" >{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">File</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">


                                <div class="input-group">
                                   <span class="input-group-btn">
                                     <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                     </a>
                                   </span>
                                    <input id="thumbnail" class="form-control" type="text" name="filepath" value="">
                                </div>
                                <img id="holder" style="margin-top:15px;max-height:100px;">

                                <input type="button" class="btn btn-default" onclick="$('#thumbnail').val(''); $('#holder').attr('src','');" value="Elimina foto scelta" />

                            </div>
                        </div>


                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection







@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <script>
        $(document).ready(function() {
            $("#id_veicolo").select2({
                placeholder: "Seleziona veicolo",
                allowClear: true
            });

            $("#id_rimorchio").select2({
                placeholder: "Seleziona rimorchio",
                allowClear: true
            });

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });

            $("#riferimento_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });
        });
    </script>
    <!-- /Select2 -->


@endsection
