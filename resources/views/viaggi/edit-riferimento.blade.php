
@extends('layouts.main')



@section('content_page')


    <h1>Modifica Riferimento</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <ul class="add_all nav navbar-right panel_toolbox">
                        <li>
                            <a href="<?php echo route("viaggi::edit",['id'=>$riferimento->id_viaggio]); ?>" class="btn btn-primary"> <i class="fa fa-chevron-left"></i> Torna al viaggio</a>
                        </li>
                    </ul>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="row">

                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <label for="numero">Numero</label>
                                <div class="">
                                    <input type="text" id="numero" value="{{ $riferimento->numero }}" name="numero" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>


                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <label for="numero">Data</label>
                                <div class="">
                                    <?php
                                    $data_ok = new \Carbon\Carbon($riferimento->data);
                                    ?>
                                    <input type="text" value="{{ $data_ok->format("d/m/Y") }}" id="data" name="data" class="form-control col-md-7 col-xs-12">
                                </div>
                            </div>

                            <div class="form-group col-md-4 col-xs-12">
                                <label for="metri_lineari">Tipo</label>
                                <div class="">
                                    <select name="tipo" class="form-control">
                                        <option value="">Seleziona tipo</option>
                                        <?php
                                        foreach(\App\Models\Tiporiferimento::all() as $tipo){
                                        ?>
                                        <option value="{{ $tipo->nome }}" <?php if($riferimento->tipo == $tipo->nome) echo "selected"; ?> >{{ $tipo->nome }}</option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="form-group col-md-4 col-xs-12">
                                <select id="azienda" name="azienda" class="form-control" tabindex="-1">
                                    <option value="">seleziona azienda</option>
                                    <?php

                                    foreach(App\Models\Azienda::orderBy('ragsoc')->get() as $az){
                                    ?>
                                    <option value="{{ $az->id }}" <?php if($riferimento->azienda == $az->id) echo "selected"; ?> >{{ $az->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group col-md-8 col-xs-12">

                            <label>File</label>

                            <div class="input-group">
                               <span class="input-group-btn">
                                 <a id="lfm2" data-input="thumbnail2" data-preview="holder" class="btn btn-primary">
                                   <i class="fa fa-picture-o"></i> Choose
                                 </a>
                               </span>
                                <input id="thumbnail2" class="form-control" type="text" name="filepath" value="{{ $riferimento->documento }}">
                            </div>

                            <input type="button" class="btn btn-default" onclick="$('#thumbnail2').val('');" value="Elimina file scelto" />


                        </div>






                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>


                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection







@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo env('APP_URL')?>/tema/production/js/moment/moment.min.js"></script>

    <!--
    <script src="<?php echo env('APP_URL')?>/tema/production/js/datepicker/daterangepicker.js"></script>
    -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script>
        $(document).ready(function() {

            $('#lfm2').filemanager('file');

            $("#id_luogo").select2({
                placeholder: "Seleziona luogo",
                allowClear: true
            });


            $('#data').daterangepicker({
                singleDatePicker: true,
                //calender_style: "picker_1",
                locale: {
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
                    "monthNames": ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
                },
            }, function(start, end, label) {
                //console.log(start.toISOString(), end.toISOString(), label);
            });



        });
    </script>
    <!-- /Select2 -->


@endsection
