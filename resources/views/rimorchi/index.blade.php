
@extends('layouts.main')



@section('content_page')


    <h1> Rimorchi</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Lista dei rimorchi</h2>
                <ul class="add_all nav navbar-right panel_toolbox">
                    <li>
                        <a href="<?php echo route('rimorchi::add')?>" class="btn btn-primary">Aggiungi un nuovo rimorchio<i class="fa fa-plus"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Targa</th>
                        <th>Nome</th>
                        <th>Azienda</th>
                        <th width="100"></th>
                        <th width="100"></th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php
                        foreach ($rimorchi as $rim){

                            $rag_soc = '';
                            if($rim->id_azienda != 0){
                                $aziendaOBJ = \App\Models\Azienda::find($rim->id_azienda);
                                if($aziendaOBJ){
                                    $rag_soc = $aziendaOBJ->ragsoc;
                                }
                            }
                    ?>
                        <tr>
                            <td>
                                <?php echo $rim->targa; ?>

                                <div class="modal fade" id="ModalConfirmDelete_{{$rim->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" style="color: #f00; font-size: 20px;">Attenzione</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Sei sicuro di voler cancellare l'elemento selezionato?
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
                                                <a href="<?php echo route('rimorchi::delete',['id' => $rim->id ]) ?>" class="btn btn-danger">Si, cancella</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </td>
                            <td><?php echo $rim->nome; ?></td>
                            <td><?php echo $rag_soc ?></td>
                            <td><a href="<?php echo route('rimorchi::edit',['id' => $rim->id])?>" class="btn btn-dark">Modifica</a></td>
                            <td>

                                <button type="button" class="btn btn-danger" data-target="#ModalConfirmDelete_{{$rim->id}}" data-toggle="modal" >Elimina</button>

                            </td>
                        </tr>

                    <?php
                        }
                    ?>




                    </tbody>
                </table>

            </div>
        </div>
    </div>






@endsection





@section('footer_script')
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo env('APP_URL')?>/tema/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>


    <!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable-responsive').DataTable();


        });
    </script>
    <!-- /Datatables -->


@endsection