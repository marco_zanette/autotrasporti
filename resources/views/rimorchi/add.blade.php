
@extends('layouts.main')



@section('content_page')


    <h1>Nuovo Rimorchio</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Inserisci i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nome" name="nome" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Targa</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="targa" name="targa" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Azienda</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="id_azienda" name="id_azienda" class="form-control" tabindex="-1">
                                    <option></option>
                                    <?php
                                    foreach(\App\Models\Azienda::all() as $azienda){
                                    ?>
                                    <option value="{{ $azienda->id }}">{{ $azienda->ragsoc }}</option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Aggiungi</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('footer_script')

    <link href="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <script src="<?php echo env('APP_URL')?>/tema/vendors/select2/dist/js/select2.full.min.js"></script>


    <script>
        $(document).ready(function() {

            $("#id_azienda").select2({
                placeholder: "Seleziona azienda",
                allowClear: true
            });
        });
    </script>
    <!-- /Select2 -->


@endsection
