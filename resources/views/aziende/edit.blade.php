
@extends('layouts.main')



@section('content_page')


    <h1>Modifica Azienda</h1>

    <?php
    \App\Utilities\AlertMsg::stampaMsg();
    ?>


    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Modifica i dati</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ragsoc">Ragione sociale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->ragsoc }}" type="text" id="racsog" name="ragsoc" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="indirizzo">Indirizzo</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->indirizzo }}" type="text" id="indirizzo" name="indirizzo" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="comune">Comune</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->comune }}"type="text" id="comune" name="comune" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cap">Cap</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->cap }}" type="text" id="cap" name="cap" data-inputmask="'mask' : '99999'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="provincia">Provincia</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->provincia }}" type="text" id="provincia" name="provincia" data-inputmask="'mask' : 'AA'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nazione">Nazione</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="nazione" name="nazione" class="form-control col-md-7 col-xs-12">
                                    <?php
                                    foreach (\App\Models\Nazione::all()->sortBy('nome') as $naz){
                                    ?>
                                    <option value="{{$naz->iso}}" <?php if($naz->iso == $azienda->nazione) echo "selected"; ?> > {{$naz->nome}} </option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="piva">P.IVA</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->piva }}" type="text" id="piva" name="piva" data-inputmask="'mask' : '***********'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codfisc">Codice fiscale</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="{{ $azienda->codfisc }}" type="text" id="codfisc" name="codfisc" data-inputmask="'mask' : '****************'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codfisc">Modalità pagamento cliente</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{ $azienda->pagamento_cliente }}" id="pagamento_cliente" name="pagamento_cliente" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codfisc">Modalità pagamento fornitore</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  value="{{ $azienda->pagamento_fornitore }}" id="pagamento_fornitore" name="pagamento_fornitore" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codfisc">Banca</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{ $azienda->banca }}" id="banca" name="banca" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="codfisc">IBAN</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text"  value="{{ $azienda->iban }}" id="iban" name="iban" data-inputmask="'mask' : '***************************'" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">E-mail</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{ $azienda->email }}" id="email" name="email" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pec">PEC</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" value="{{ $azienda->pec }}" id="pec" name="pec" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Modifica</button>
                            </div>
                        </div>

                        {{ csrf_field() }}

                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

