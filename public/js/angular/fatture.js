

var fattureModule = angular.module('FattureApp', []);


// runtime configuro le funzioni globali
fattureModule.run(["$rootScope",function($rootScope) {




}]);






fattureModule.controller('GestisciFattura', ['$rootScope','$scope',function($rootScope,$scope) {

    $scope.viaggi = viaggi;
    $scope.iva_applicata = iva_applicata;


    $scope.add = function(index){
        $scope.viaggi.push(viaggi_sel[index]);

        $("#modal_viaggi").modal('hide');
    }



    $scope.addSelezionati = function(index){

        $( "input.sel_add_viaggi:checked" ).each(function( index ) {
            $scope.add($( this ).val());
        });
    }


    $scope.remove = function (index) {
        $scope.viaggi.splice(index, 1);
    }


    $scope.imponibile = function () {

        var imponibile = 0;

        for(var a=0; a<$scope.viaggi.length; a++){
            imponibile = imponibile+parseFloat($scope.viaggi[a].prezzo);
        }

        return imponibile;
    }



    $scope.iva = function () {


        if(typeof ($scope.iva_applicata) == "undefined"){
            return 0;
        } else {
            var iva_applicata = parseFloat($scope.iva_applicata);
        }

        return ($scope.imponibile()*iva_applicata)/100;
    }



    $scope.totale = function () {


        return parseFloat($scope.imponibile())+parseFloat($scope.iva());
    }



    $scope.submit = function () {
        document.getElementById("form_fattura").submit();
    }




}]);
