<?php


/**
 * Class AlertMsg
 * classe per gestire i messaggi da stampare a video per post form oppure errori DB...
 *
 * $_SESSION['alert_msg']: array contenente tutti i messaggi
 * Struttura:
 *      $_SESSION['alert_msg'][0] = array(
 *          'msg'  => 'messaggio',
 *          'tipo' => XX,
 *          'priorita' => YY
 *      )
 *
 */

namespace App\Utilities;


class AlertMsg {


    const TIPO_ERROR = "error";
    const TIPO_WARNING = "warning";
    const TIPO_SUCCESS = "success";

    const PRIORITA_ALTA = 1;
    const PRIORITA_MEDIA = 2;
    const PRIORITA_BASSA = 3;

    // varibile per verificare sia la prima istanza e quindi pulire l'array
    private static $singleton = false;


    /**
     * Setta un messaggio
     *
     * @param $msg
     * @param string $tipo
     * @param int $priorita
     */
    public static function setMsg($msg,$tipo=self::TIPO_SUCCESS,$priorita=self::PRIORITA_MEDIA){
        if(self::$singleton === false){ // pulisco l'array se è la prima istanza
            request()->session()->put('alert_msg',array());
            self::$singleton = true;
        }

        request()->session()->push('alert_msg',array(
            'msg'  => $msg,
            'tipo' => $tipo,
            'priorita' => $priorita
        ));
    }



    /**
     * Ritorna i messaggi
     *
     * @return mixed
     */
    public static function getMsg(){
        return request()->get('alert_msg');
    }




    public static function stampaMsg(){

        if(!is_array(request()->session()->get('alert_msg'))) request()->session()->put('alert_msg',array());
        $html = "";


        foreach(request()->session()->get('alert_msg') as $messaggio){

            // fix tipo nella stringa corretta bootstrap
            switch($messaggio['tipo']){
                case "error":
                    $tipo_msg = "danger";
                    break;
                case "warning":
                    $tipo_msg = "warning";
                    break;
                case "success":
                    $tipo_msg = "success";
                    break;
                default:
                    $tipo_msg = "info";
            }

            $html = "<div role='alert' class='alert alert-".$tipo_msg."'>".$messaggio['msg']."<a class='close' onclick='$(this).parent().remove();'>×</a></div>";
        }

        echo $html;

        // pulisco la sessione
        request()->session()->put('alert_msg',array());
    }



}