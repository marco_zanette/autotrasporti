<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viaggiovuoto extends Model
{


    protected $table = 'viaggivuoti';




    public function azienda()
    {
        return $this->belongsTo('App\Models\Azienda', 'id_azienda');
    }


    public function veicolo()
    {
        return $this->belongsTo('App\Models\Veicolo', 'id_veicolo');
    }

    public function rimorchio()
    {
        return $this->belongsTo('App\Models\Rimorchio', 'id_rimorchio');
    }


    public function partenza(){
        return $this->belongsTo('App\Models\Luogo', 'p_luogo');
    }

    public function arrivo(){
        return $this->belongsTo('App\Models\Luogo', 'a_luogo');
    }




}



