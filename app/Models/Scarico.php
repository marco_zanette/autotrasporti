<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scarico extends Model
{


    protected $table = 'scarichi';


    protected $casts = [
        'carichi' => 'array',
    ];



    public function luogo()
    {
        return $this->belongsTo('App\Models\Luogo', 'id_luogo');
    }

}



