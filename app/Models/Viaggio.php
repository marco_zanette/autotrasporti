<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Viaggio extends Model
{


    protected $table = 'viaggi';




    public function scarichi()
    {
        return $this->hasMany('App\Models\Scarico', 'id_viaggio');
    }


    public function riferimenti()
    {
        return $this->hasMany('App\Models\Riferimentoviaggio', 'id_viaggio');
    }


    public function riferimentoAzienda()
    {
        return $this->belongsTo('App\Models\Azienda', 'riferimento_azienda');
    }



}



