<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;





class ViaggivuotiController extends Controller
{





    public function index(Request $request)
    {

        $viaggi = \App\Models\Viaggiovuoto::all();


        return view('viaggivuoti.index',['viaggi'=>$viaggi]);
    }






    public function add(Request $request)
    {


        if($request->isMethod('post')) { // gestisco post form


            $p_dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("p_data"));
            $a_dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("a_data"));

            $viaggioOBJ = new \App\Models\Viaggiovuoto();
            $viaggioOBJ->id_veicolo = (int)$request->input('id_veicolo');
            $viaggioOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
            $viaggioOBJ->id_azienda = (int)$request->input('id_azienda');

            $viaggioOBJ->p_data = $p_dataok;
            $viaggioOBJ->p_luogo = (int)$request->input('p_luogo');
            $viaggioOBJ->p_km = (int)$request->input('p_km');
            $viaggioOBJ->p_ora = $request->input('p_ora');
            $viaggioOBJ->a_data = $a_dataok;
            $viaggioOBJ->a_luogo = (int)$request->input('a_luogo');
            $viaggioOBJ->a_km = (int)$request->input('a_km');
            $viaggioOBJ->a_ora = $request->input('a_ora');

            $viaggioOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Viaggio inserito');

            return redirect()->route('viaggivuoti::index');

        }


        return view('viaggivuoti.add');
    }






    public function edit(Request $request,$id)
    {

        $viaggioOBJ = \App\Models\Viaggiovuoto::find($id);


        if($request->isMethod('post')) { // gestisco post form

            $p_dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("p_data"));
            $a_dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("a_data"));

            $viaggioOBJ->id_veicolo = (int)$request->input('id_veicolo');
            $viaggioOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
            $viaggioOBJ->id_azienda = (int)$request->input('id_azienda');

            $viaggioOBJ->p_data = $p_dataok;
            $viaggioOBJ->p_luogo = (int)$request->input('p_luogo');
            $viaggioOBJ->p_km = (int)$request->input('p_km');
            $viaggioOBJ->p_ora = $request->input('p_ora');
            $viaggioOBJ->a_data = $a_dataok;
            $viaggioOBJ->a_luogo = (int)$request->input('a_luogo');
            $viaggioOBJ->a_km = (int)$request->input('a_km');
            $viaggioOBJ->a_ora = $request->input('a_ora');

            $viaggioOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Viaggio modificato');

            return redirect()->refresh();
        }


        return view('viaggivuoti.edit',['viaggio'=>$viaggioOBJ]);

    }



    public function delete(Request $request, $id){

        \App\Models\Viaggiovuoto::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Viaggio eliminato');

        return redirect()->route('viaggivuoti::index');


    }




}

