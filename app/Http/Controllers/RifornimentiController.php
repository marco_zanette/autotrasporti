<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;





class RifornimentiController extends Controller
{

    public function index(Request $request)
    {

        $rifornimenti = \App\Models\Rifornimento::all();


        return view('rifornimenti.index',['rifornimenti'=>$rifornimenti]);
    }






    public function add(Request $request)
    {


        if($request->isMethod('post')) { // gestisco post form

            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


            $rifornimentoOBJ = new \App\Models\Rifornimento();
            $rifornimentoOBJ->id_veicolo = (int)$request->input('id_veicolo');
            //$rifornimentoOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
            $rifornimentoOBJ->id_rimorchio = 0;
            $rifornimentoOBJ->id_luogo = (int)$request->input('id_luogo');
            $rifornimentoOBJ->id_fattura = (int)$request->input('id_fattura');
            $rifornimentoOBJ->data = $dataok;
            $rifornimentoOBJ->km = $request->input('km');
            $rifornimentoOBJ->prodotto = $request->input('prodotto');
            $rifornimentoOBJ->quantita = $request->input('quantita');
            $rifornimentoOBJ->quantita_tipo = $request->input('quantita_tipo');
            $rifornimentoOBJ->prezzo_unitario = $request->input('prezzo_unitario');
            $rifornimentoOBJ->imponibile = $request->input('imponibile');
            $rifornimentoOBJ->iva = $request->input('iva');
            $rifornimentoOBJ->totale = $request->input('totale');
            $rifornimentoOBJ->documento1 = " ";
            $rifornimentoOBJ->documento2 = " ";
            $rifornimentoOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Aggiunto rifornimento');

            return redirect()->route('rifornimenti::index');

        }


        return view('rifornimenti.add');
    }






    public function edit(Request $request,$id)
    {

        $rifornimentoOBJ = \App\Models\Rifornimento::find($id);



        if($request->isMethod('post')) { // gestisco post form

            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));

            $rifornimentoOBJ->id_veicolo = (int)$request->input('id_veicolo');
            //$rifornimentoOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
            $rifornimentoOBJ->id_rimorchio = 0;
            $rifornimentoOBJ->id_luogo = (int)$request->input('id_luogo');
            $rifornimentoOBJ->id_fattura = (int)$request->input('id_fattura');
            $rifornimentoOBJ->data = $dataok;
            $rifornimentoOBJ->km = $request->input('km');
            $rifornimentoOBJ->prodotto = $request->input('prodotto');
            $rifornimentoOBJ->quantita = $request->input('quantita');
            $rifornimentoOBJ->quantita_tipo = $request->input('quantita_tipo');
            $rifornimentoOBJ->prezzo_unitario = $request->input('prezzo_unitario');
            $rifornimentoOBJ->imponibile = $request->input('imponibile');
            $rifornimentoOBJ->iva = $request->input('iva');
            $rifornimentoOBJ->totale = $request->input('totale');
            $rifornimentoOBJ->documento1 = " ";
            $rifornimentoOBJ->documento2 = " ";
            $rifornimentoOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Rifornimento modificato');

            return redirect()->refresh();

        }


        return view('rifornimenti.edit',['rifornimenti'=>$rifornimentoOBJ]);

    }



    public function delete(Request $request, $id){

        \App\Models\Rifornimento::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Rifornimento eliminato');

        return redirect()->route('rifornimenti::index');


    }
}

