<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;


class ViaggiController extends Controller
{



    public function index(Request $request)
    {

        $viaggi = \App\Models\Viaggio::all();


        return view('viaggi.index',['viaggi'=>$viaggi]);
    }





    public function add(Request $request)
    {

        if($request->isMethod('post')) {

            $numero = 0;
            $ultimoV = \App\Models\Viaggio::orderBy('numero','desc')->first();
            if($ultimoV){
                $numero = $ultimoV->numero+1;
            } else {
                $numero = 1;
            }

            /*
            $ultimoV = \App\Models\Ordine::orderBy('numero','desc')->first();
            if($ultimoV){
                $numero = $ultimoV->numero+1;
            }
            */


            $viaggioOBJ = new \App\Models\Viaggio();
            $viaggioOBJ->numero = $numero;
            $viaggioOBJ->id_veicolo = (int)$request->input('id_veicolo');
            $viaggioOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
            $viaggioOBJ->id_azienda = (int)$request->input('id_azienda');
            $viaggioOBJ->prezzo = (int)$request->input('prezzo');

            $viaggioOBJ->riferimento_n = (int)$request->input('riferimento_n');
            $viaggioOBJ->riferimento_tipo = $request->input('riferimento_tipo');
            $viaggioOBJ->riferimento_tipo = '';
            $viaggioOBJ->riferimento_azienda = (int)$request->input('riferimento_azienda');

            $viaggioOBJ->riferimento_doc = $request->input("filepath");

            $viaggioOBJ->save();


            return redirect()->route("viaggi::edit",['id'=>$viaggioOBJ->id]);

        }


        return view('viaggi.add');
    }






    public function delete(Request $request,$id)
    {

        $viaggioOBJ = \App\Models\Viaggio::find($id);
        $viaggioOBJ->delete();

        \App\Models\Scarico::where('id_viaggio', $id)->delete();
        \App\Models\Carico::where('id_viaggio', $id)->delete();


        \App\Utilities\AlertMsg::setMsg("Viaggio eliminato");
        return \redirect()->route("viaggi::index");
    }





    public function duplicate(Request $request,$id)
    {

        $numero = 0;
        $ultimoV = \App\Models\Viaggio::orderBy('numero','desc')->first();
        $numero = $ultimoV->numero+1;

        $viaggioOBJ = \App\Models\Viaggio::find($id);
        $viaggioOBJ_new = $viaggioOBJ->replicate();
        $viaggioOBJ_new->numero = $numero;
        $viaggioOBJ_new->save();


        // duplico i carichi
        foreach (\App\Models\Carico::where('id_viaggio', $id)->get() as $caricoOBJ){
            $caricoOBJ_new = $caricoOBJ->replicate();
            $caricoOBJ_new->id_viaggio = $viaggioOBJ_new->id;
            $caricoOBJ_new->save();
        }




        \App\Utilities\AlertMsg::setMsg("Viaggio duplicato");
        return \redirect()->route("viaggi::edit",['id'=>$viaggioOBJ_new->id]);
    }












    public function edit(Request $request,$id)
    {

        $viaggioOBJ = \App\Models\Viaggio::find($id);


        if($request->isMethod('post')) { // gestisco post form


            // modifica viaggio
            if($request->exists("edit_viaggio") and $request->input("edit_viaggio") == 1) {

                $viaggioOBJ->id_veicolo = (int)$request->input('id_veicolo');
                $viaggioOBJ->id_rimorchio = (int)$request->input('id_rimorchio');
                $viaggioOBJ->id_azienda = (int)$request->input('id_azienda');
                $viaggioOBJ->prezzo = (int)$request->input('prezzo');
                $viaggioOBJ->riferimento_n = (int)$request->input('riferimento_n');
                $viaggioOBJ->riferimento_tipo = $request->input('riferimento_tipo');
                //$viaggioOBJ->riferimento_tipo = '';
                $viaggioOBJ->riferimento_azienda = (int)$request->input('riferimento_azienda');
                $viaggioOBJ->riferimento_doc = $request->input("filepath");

                $viaggioOBJ->save();

                \App\Utilities\AlertMsg::setMsg("Dati viaggio modificati");
                return Redirect::refresh();
            }




            // aggiunta riferimento
            if($request->exists("add_riferimento") and $request->input("add_riferimento") == 1){

                $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));

                $riferimento = new \App\Models\Riferimentoviaggio();
                $riferimento->id_viaggio = $viaggioOBJ->id;
                $riferimento->numero = $request->input('numero');
                $riferimento->data = $dataok;
                $riferimento->tipo = $request->input('tipo');
                $riferimento->azienda = (int)$request->input('azienda');
                $riferimento->documento = $request->input("filepath");
                $riferimento->save();

                \App\Utilities\AlertMsg::setMsg("Riferimento inserito");
                return Redirect::refresh();
            }




            // aggiunta carico
            if($request->exists("add_carico") and $request->input("add_carico") == 1){

                //$dataok = new \Carbon\Carbon($request->input("data"));

                $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


                $caricoOBJ = new \App\Models\Carico();
                $caricoOBJ->id_viaggio = $viaggioOBJ->id;
                $caricoOBJ->data = $dataok;
                $caricoOBJ->id_luogo = (int)$request->input("id_luogo");
                $caricoOBJ->km = $request->input("km");
                $caricoOBJ->ora_arrivo = $request->input("ora_arrivo");
                $caricoOBJ->ora_inizio_carico = $request->input("ora_inizio_carico");
                $caricoOBJ->ora_fine_carico = $request->input("ora_fine_carico");
                $caricoOBJ->note = $request->input("note");
                $caricoOBJ->tipo_merce = $request->input("tipo_merce");
                $caricoOBJ->quantita = $request->input("quantita");
                $caricoOBJ->metri_lineari = (float)$request->input("metri_lineari");
                $caricoOBJ->volume = (float)$request->input("volume");
                $caricoOBJ->peso = (float)$request->input("peso");

                //$caricoOBJ->documento1 = $request->input("filepath");
                $caricoOBJ->documento1 = "";
                $caricoOBJ->documento2 = "";
                $caricoOBJ->documento3 = "";
                $caricoOBJ->documento4 = "";
                $caricoOBJ->documento5 = "";

                $caricoOBJ->save();


                \App\Utilities\AlertMsg::setMsg("Carico inserito");
                return Redirect::refresh();
            }



            // aggiunta scarico
            if($request->exists("add_scarico") and $request->input("add_scarico") == 1){


                $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


                $caricoOBJ = new \App\Models\Scarico();
                $caricoOBJ->id_viaggio = $viaggioOBJ->id;
                $caricoOBJ->data = $dataok;
                $caricoOBJ->id_luogo = (int)$request->input("id_luogo");
                $caricoOBJ->km = (int)$request->input("km");
                $caricoOBJ->ora_arrivo = $request->input("ora_arrivo");
                $caricoOBJ->ora_inizio_carico = $request->input("ora_inizio_carico");
                $caricoOBJ->ora_fine_carico = $request->input("ora_fine_carico");
                $caricoOBJ->note = $request->input("note");
                /*
                $caricoOBJ->tipo_merce = $request->input("tipo_merce");
                $caricoOBJ->quantita = $request->input("quantita");
                $caricoOBJ->metri_lineari = $request->input("metri_lineari");
                $caricoOBJ->volume = $request->input("volume");
                $caricoOBJ->peso = $request->input("peso");
                */

                if($request->get("carichi")){
                    $carichi = $request->get("carichi");
                } else {
                    $carichi = array();
                }


                $caricoOBJ->tipo_merce = "";
                $caricoOBJ->quantita = "";
                $caricoOBJ->metri_lineari = 0;
                $caricoOBJ->volume = 0;
                $caricoOBJ->peso = 0;
                $caricoOBJ->carichi = $carichi;
                $caricoOBJ->documento1 = "";
                $caricoOBJ->documento2 = "";
                $caricoOBJ->documento3 = "";
                $caricoOBJ->documento4 = "";
                $caricoOBJ->documento5 = "";


                $caricoOBJ->save();


                \App\Utilities\AlertMsg::setMsg("Scarico inserito");
                return Redirect::refresh();
            }



        }


        return view('viaggi.edit',['viaggio'=>$viaggioOBJ]);

    }









    public function scaricoEdit(Request $request,$id){



        $caricoOBJ = \App\Models\Scarico::find($id);



        if($request->isMethod('post')) { // gestisco post form


            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


            if($request->get("carichi")){
                $carichi = $request->get("carichi");
            } else {
                $carichi = array();
            }

            $caricoOBJ->data = $dataok;
            $caricoOBJ->id_luogo = (int)$request->input("id_luogo");
            $caricoOBJ->km = $request->input("km");
            $caricoOBJ->ora_arrivo = $request->input("ora_arrivo");
            $caricoOBJ->ora_inizio_carico = $request->input("ora_inizio_carico");
            $caricoOBJ->ora_fine_carico = $request->input("ora_fine_carico");
            $caricoOBJ->note = $request->input("note");
            $caricoOBJ->tipo_merce = '';
            $caricoOBJ->quantita = 0;
            $caricoOBJ->metri_lineari = 0;
            $caricoOBJ->volume = 0;
            $caricoOBJ->peso = 0;
            $caricoOBJ->carichi = $carichi;

            $caricoOBJ->documento1 = "";
            $caricoOBJ->documento2 = "";
            $caricoOBJ->documento3 = "";
            $caricoOBJ->documento4 = "";
            $caricoOBJ->documento5 = "";

            $caricoOBJ->save();

            \App\Utilities\AlertMsg::setMsg("Scarico modificato");

            return redirect()->refresh();
        }



        return view('viaggi.edit-scarico',['carico'=>$caricoOBJ]);

    }








    public function scaricoDelete(Request $request,$id){



        \App\Models\Scarico::destroy($id);

        \App\Utilities\AlertMsg::setMsg("Scarico eliminato");

        return Redirect::back();

    }











    public function caricoEdit(Request $request,$id){



        $caricoOBJ = \App\Models\Carico::find($id);



        if($request->isMethod('post')) { // gestisco post form


            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


            $caricoOBJ->data = $dataok;
            $caricoOBJ->id_luogo = (int)$request->input("id_luogo");
            $caricoOBJ->km = $request->input("km");
            $caricoOBJ->ora_arrivo = $request->input("ora_arrivo");
            $caricoOBJ->ora_inizio_carico = $request->input("ora_inizio_carico");
            $caricoOBJ->ora_fine_carico = $request->input("ora_fine_carico");
            $caricoOBJ->note = $request->input("note");
            $caricoOBJ->tipo_merce = $request->input("tipo_merce");
            $caricoOBJ->quantita = $request->input("quantita");
            $caricoOBJ->metri_lineari = (float)$request->input("metri_lineari");
            $caricoOBJ->volume = (float)$request->input("volume");
            $caricoOBJ->peso = (float)$request->input("peso");

            $caricoOBJ->documento1 = "";
            $caricoOBJ->documento2 = "";
            $caricoOBJ->documento3 = "";
            $caricoOBJ->documento4 = "";
            $caricoOBJ->documento5 = "";

            $caricoOBJ->save();

            \App\Utilities\AlertMsg::setMsg("Carico modificato");

            return redirect()->refresh();
        }



        return view('viaggi.edit-carico',['carico'=>$caricoOBJ]);

    }












    public function caricoDelete(Request $request,$id){



        \App\Models\Carico::destroy($id);

        \App\Utilities\AlertMsg::setMsg("Carico eliminato");

        return Redirect::back();

    }













    public function riferimentoEdit(Request $request,$id){



        $riferimentoOBJ = \App\Models\Riferimentoviaggio::find($id);



        if($request->isMethod('post')) { // gestisco post form


            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));

            $riferimentoOBJ->numero = $request->input('numero');
            $riferimentoOBJ->data = $dataok;
            $riferimentoOBJ->tipo = $request->input('tipo');
            $riferimentoOBJ->azienda = (int)$request->input('azienda');
            $riferimentoOBJ->documento = $request->input("filepath");
            $riferimentoOBJ->save();

            \App\Utilities\AlertMsg::setMsg("Riferimento inserito");
            return Redirect::refresh();
        }



        return view('viaggi.edit-riferimento',['riferimento'=>$riferimentoOBJ]);

    }





    public function riferimentoDelete(Request $request,$id){



        \App\Models\Riferimentoviaggio::destroy($id);

        \App\Utilities\AlertMsg::setMsg("Riferimento eliminato");

        return Redirect::back();

    }




}

