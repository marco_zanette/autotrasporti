<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use PDF;




class OrdiniController extends Controller
{



    public function index(Request $request)
    {

        $ordini = \App\Models\Ordine::all();


        return view('ordini.index',['ordini'=>$ordini]);
    }





    public function add(Request $request)
    {

        if($request->isMethod('post')) {

            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));


            $dataok_carico = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("carico_data"));
            $dataok_scarico = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scarico_data"));



            $numero = 0;
            $ultimoV = \App\Models\Viaggio::orderBy('numero','desc')->first();
            if($ultimoV){
                $numero = $ultimoV->numero+1;
            } else {
                $numero = 1;
            }

            $ultimoV = \App\Models\Ordine::orderBy('numero','desc')->first();
            if($ultimoV){
                $numero = $ultimoV->numero+1;
            }


            $fatOBJ = new \App\Models\Ordine();
            $fatOBJ->data = $dataok;
            //$fatOBJ->numero = $request->input('numero');;
            $fatOBJ->numero = $numero;
            $fatOBJ->id_azienda = $request->input('id_azienda');
            $fatOBJ->id_luogo_carico = $request->input('id_luogo_carico');
            $fatOBJ->id_luogo_scarico = $request->input('id_luogo_scarico');
            $fatOBJ->prezzo = (float)$request->input('prezzo');
            $fatOBJ->merce = $request->input('merce');
            $fatOBJ->carico = $request->input('carico');
            $fatOBJ->ml = (float)$request->input('ml');
            $fatOBJ->mc = (float)$request->input('mc');
            $fatOBJ->kg = (float)$request->input('kg');
            $fatOBJ->veicolo = $request->input('veicolo');
            $fatOBJ->carico_data = $dataok_carico;
            $fatOBJ->carico_ora = $request->input('carico_ora');
            $fatOBJ->scarico_data = $dataok_scarico;
            $fatOBJ->scarico_ora = $request->input('scarico_ora');
            $fatOBJ->pagamento = $request->input('pagamento');
            $fatOBJ->save();



            // aggiungo viaggio vuoto automaticamente
            $viaggioOBJ = new \App\Models\Viaggio();
            $viaggioOBJ->numero = ($numero);
            $viaggioOBJ->id_veicolo = 0;
            $viaggioOBJ->id_rimorchio = 0;
            $viaggioOBJ->id_azienda = 0;
            $viaggioOBJ->prezzo = 0;
            $viaggioOBJ->riferimento_n = 0;
            $viaggioOBJ->riferimento_tipo = '';
            $viaggioOBJ->riferimento_tipo = '';
            $viaggioOBJ->riferimento_azienda = 0;
            $viaggioOBJ->riferimento_doc = '';
            $viaggioOBJ->from_ordine = 1;
            $viaggioOBJ->save();


            // aggiungo carico viaggio vuoto automaticamente
            $caricoOBJ = new \App\Models\Carico();
            $caricoOBJ->id_viaggio = $viaggioOBJ->id;
            $caricoOBJ->data = $dataok;
            $caricoOBJ->id_luogo = (int)$request->input('id_luogo_carico');
            $caricoOBJ->km = 0;
            $caricoOBJ->ora_arrivo = '00:00';
            $caricoOBJ->ora_inizio_carico = '00:00';
            $caricoOBJ->ora_fine_carico = '00:00';
            $caricoOBJ->note = '';
            $caricoOBJ->tipo_merce = '';
            $caricoOBJ->quantita = 0;
            $caricoOBJ->metri_lineari = 0;
            $caricoOBJ->volume = 0;
            $caricoOBJ->peso = 0;
            $caricoOBJ->documento1 = "";
            $caricoOBJ->documento2 = "";
            $caricoOBJ->documento3 = "";
            $caricoOBJ->documento4 = "";
            $caricoOBJ->documento5 = "";
            $caricoOBJ->save();



            $caricoOBJ = new \App\Models\Scarico();
            $caricoOBJ->id_viaggio = $viaggioOBJ->id;
            $caricoOBJ->data = $dataok;
            $caricoOBJ->id_luogo = (int)$request->input('id_luogo_scarico');
            $caricoOBJ->km = 0;
            $caricoOBJ->ora_arrivo = '00:00';
            $caricoOBJ->ora_inizio_carico = '00:00';
            $caricoOBJ->ora_fine_carico = '00:00';
            $caricoOBJ->note = '';
            $caricoOBJ->tipo_merce = "";
            $caricoOBJ->quantita = "";
            $caricoOBJ->metri_lineari = 0;
            $caricoOBJ->volume = 0;
            $caricoOBJ->peso = 0;
            $caricoOBJ->carichi = array();
            $caricoOBJ->documento1 = "";
            $caricoOBJ->documento2 = "";
            $caricoOBJ->documento3 = "";
            $caricoOBJ->documento4 = "";
            $caricoOBJ->documento5 = "";
            $caricoOBJ->save();


            \App\Utilities\AlertMsg::setMsg('Ordine inserito');

            return redirect()->route('ordini::index');

        }


        return view('ordini.add');
    }






    public function edit(Request $request,$id)
    {

        $fatOBJ = \App\Models\Ordine::find($id);


        if($request->isMethod('post')) { // gestisco post form


            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));
            $dataok_carico = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("carico_data"));
            $dataok_scarico = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scarico_data"));


            $fatOBJ->data = $dataok;
            //$fatOBJ->numero = $request->input('numero');;
            $fatOBJ->id_azienda = $request->input('id_azienda');
            $fatOBJ->id_luogo_carico = $request->input('id_luogo_carico');
            $fatOBJ->id_luogo_scarico = $request->input('id_luogo_scarico');
            $fatOBJ->prezzo = (float)$request->input('prezzo');
            $fatOBJ->merce = $request->input('merce');
            $fatOBJ->carico = $request->input('carico');
            $fatOBJ->ml = (float)$request->input('ml');
            $fatOBJ->mc = (float)$request->input('mc');
            $fatOBJ->kg = (float)$request->input('kg');
            $fatOBJ->veicolo = $request->input('veicolo');
            $fatOBJ->carico_data = $dataok_carico;
            $fatOBJ->carico_ora = $request->input('carico_ora');
            $fatOBJ->scarico_data = $dataok_scarico;
            $fatOBJ->scarico_ora = $request->input('scarico_ora');
            $fatOBJ->pagamento = $request->input('pagamento');
            $fatOBJ->save();

            $fatOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Ordine modificato');


            return redirect()->refresh();

        }


        return view('ordini.edit',['ordine'=>$fatOBJ]);

    }










    public function pdf(Request $request,$id)
    {

        $fatOBJ = \App\Models\Ordine::find($id);




        $pdf = PDF::loadView('pdf.ordine', $fatOBJ);
        return $pdf->stream('document.pdf');


    }





    public function delete(Request $request, $id){

        \App\Models\Ordine::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Ordine eliminato');

        return redirect()->route('ordini::index');


    }






    public function duplicate(Request $request, $id){

        $obj_or = \App\Models\Ordine::find(1);
        $obj_new = $obj_or->replicate();
        $obj_new->save();


        \App\Utilities\AlertMsg::setMsg('Ordine duplicato');


        return redirect()->route('ordini::edit',['id'=>$obj_new->id]);


    }


}

