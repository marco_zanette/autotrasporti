<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;





class VeicoliController extends Controller
{





    public function index(Request $request)
    {

        $veicoli = \App\Models\Veicolo::all();


        return view('veicoli.index',['veicoli'=>$veicoli]);
    }






    public function add(Request $request)
    {


        if($request->isMethod('post')) { // gestisco post form

            // controllo duplicato
            $checkRecord = \App\Models\Veicolo::where("targa",$request->input('targa'))->first();
            if($checkRecord){
                \App\Utilities\AlertMsg::setMsg('Veicolo già presente',\App\Utilities\AlertMsg::TIPO_ERROR);
                return redirect()->back();
            }


            $veicoloOBJ = new \App\Models\Veicolo();
            $veicoloOBJ->targa = $request->input('targa');
            $veicoloOBJ->nome = $request->input('nome');
            $veicoloOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Veicolo inserito');

            return redirect()->route('veicoli::index');

        }


        return view('veicoli.add');
    }






    public function edit(Request $request,$id)
    {

        $veicoloOBJ = \App\Models\Veicolo::find($id);


        if($request->isMethod('post')) { // gestisco post form

            $veicoloOBJ->targa = $request->input('targa');
            $veicoloOBJ->nome = $request->input('nome');
            $veicoloOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Veicolo modificato');

            return redirect()->refresh();
        }


        return view('veicoli.edit',['veicolo'=>$veicoloOBJ]);

    }



    public function delete(Request $request, $id){

        \App\Models\Veicolo::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Veicolo eliminato');

        return redirect()->route('veicoli::index');


    }




}

