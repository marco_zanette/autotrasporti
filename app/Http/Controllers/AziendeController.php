<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;





class AziendeController extends Controller
{

    public function index(Request $request)
    {

        $aziende = \App\Models\Azienda::all();


        return view('aziende.index',['aziende'=>$aziende]);
    }



    public function add(Request $request)
    {


        if($request->isMethod('post')) { // gestisco post form


            // controllo duplicato solo se piva compilata
            if($request->input('piva') != ""){
                $checkAzienda = \App\Models\Azienda::where("piva",$request->input('piva'))->first();
                if($checkAzienda){
                    \App\Utilities\AlertMsg::setMsg('Azienda già presente',\App\Utilities\AlertMsg::TIPO_ERROR);
                    return redirect()->back();
                }
            }


            $aziendaOBJ = new \App\Models\Azienda();
            $aziendaOBJ->ragsoc = $request->input('ragsoc');
            $aziendaOBJ->indirizzo = $request->input('indirizzo');
            $aziendaOBJ->comune = $request->input('comune');
            $aziendaOBJ->cap = $request->input('cap');
            $aziendaOBJ->provincia = $request->input('provincia');
            $aziendaOBJ->nazione = $request->input('nazione');
            $aziendaOBJ->piva = $request->input('piva');
            $aziendaOBJ->codfisc = $request->input('codfisc');
            $aziendaOBJ->pagamento_cliente = $request->input('pagamento_cliente');
            $aziendaOBJ->pagamento_fornitore = $request->input('pagamento_fornitore');
            $aziendaOBJ->banca = $request->input('banca');
            $aziendaOBJ->iban = $request->input('iban');
            $aziendaOBJ->email = $request->input('email');
            $aziendaOBJ->pec = $request->input('pec');

            $aziendaOBJ->save();


            if($request->has("nuovoluogo") and $request->input('nuovoluogo') == 1){

                $luogoOBJ = new \App\Models\Luogo();
                $luogoOBJ->nome = "Sede ";
                $luogoOBJ->indirizzo = $request->input('indirizzo');
                $luogoOBJ->comune = $request->input('comune');
                $luogoOBJ->cap = $request->input('cap');
                $luogoOBJ->provincia = $request->input('provincia');
                $luogoOBJ->nazione = $request->input('nazione');
                $luogoOBJ->id_azienda = $aziendaOBJ->id;
                $luogoOBJ->save();

            }


            \App\Utilities\AlertMsg::setMsg('Aggiunta azienda');

            return redirect()->route('aziende::index');

        }


        return view('aziende.add');
    }






    public function edit(Request $request,$id)
    {

        $aziendaOBJ = \App\Models\Azienda::find($id);


        if($request->isMethod('post')) { // gestisco post form

            $aziendaOBJ->ragsoc = $request->input('ragsoc');
            $aziendaOBJ->indirizzo = $request->input('indirizzo');
            $aziendaOBJ->comune = $request->input('comune');
            $aziendaOBJ->cap = $request->input('cap');
            $aziendaOBJ->provincia = $request->input('provincia');
            $aziendaOBJ->nazione = $request->input('nazione');
            $aziendaOBJ->piva = $request->input('piva');
            $aziendaOBJ->codfisc = $request->input('codfisc');
            $aziendaOBJ->pagamento_cliente = $request->input('pagamento_cliente');
            $aziendaOBJ->pagamento_fornitore = $request->input('pagamento_fornitore');
            $aziendaOBJ->banca = $request->input('banca');
            $aziendaOBJ->iban = $request->input('iban');
            $aziendaOBJ->email = $request->input('email');
            $aziendaOBJ->pec = $request->input('pec');
            $aziendaOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Azienda modificata');

            return redirect()->refresh();

        }


        return view('aziende.edit',['azienda'=>$aziendaOBJ]);

    }



    public function delete(Request $request, $id){

        \App\Models\Azienda::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Azienda eliminata');

        return redirect()->route('aziende::index');


    }
}

