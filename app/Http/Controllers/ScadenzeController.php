<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class ScadenzeController extends Controller
{



    public function index(Request $request)
    {


        \Illuminate\Support\Facades\DB::setFetchMode(\PDO::FETCH_ASSOC);

        $qRif = "
        SELECT id,data,totale AS importo, 'rifornimento' AS titolo, 'uscita' AS tipo, 'rifornimento' AS entita
        FROM rifornimenti
        WHERE data >= '".date('Y-m-d')."'
        ";
        $rifornimenti = \DB::select($qRif);


        $scadenze = collect();
        foreach ($rifornimenti as $rif){
            //$scadenze->push($rif);
        }




        $qFat = "
        SELECT id,scadenza1 AS data,scadenza1_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza1_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza2 AS data,scadenza2_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza2_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza3 AS data,scadenza3_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza3_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }







        // fatture acquisto
        $qFat = "
        SELECT id,scadenza1 AS data,scadenza1_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza1_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza2 AS data,scadenza2_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza2_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza3 AS data,scadenza3_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 >= '".date('Y-m-d')."' AND scadenza3_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }





        return view('scadenze.index',['scadenze'=>$scadenze]);
    }

















    public function scadenzePassate(Request $request)
    {


        \Illuminate\Support\Facades\DB::setFetchMode(\PDO::FETCH_ASSOC);

        $qRif = "
        SELECT id,data,totale AS importo, 'rifornimento' AS titolo, 'uscita' AS tipo, 'rifornimento' AS entita
        FROM rifornimenti
        WHERE data < '".date('Y-m-d')."'
        ";
        $rifornimenti = \DB::select($qRif);


        $scadenze = collect();
        foreach ($rifornimenti as $rif){
            //$scadenze->push($rif);
        }




        $qFat = "
        SELECT id,scadenza1 AS data,scadenza1_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza1_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza2 AS data,scadenza2_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza2_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza3 AS data,scadenza3_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'entrata' AS tipo, 'fattura' AS entita
        FROM fatture
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza3_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }







        // fatture acquisto
        $qFat = "
        SELECT id,scadenza1 AS data,scadenza1_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza1_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza2 AS data,scadenza2_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza2_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }


        $qFat = "
        SELECT id,scadenza3 AS data,scadenza3_importo AS importo, CONCAT('Fattura n. ',numero) AS titolo, 'uscita' AS tipo, 'fattura_acquisto' AS entita
        FROM fatture_acquisto
        WHERE scadenza1 < '".date('Y-m-d')."' AND scadenza3_importo <> 0
        ";
        $fatture = \DB::select($qFat);
        foreach ($fatture as $fat){
            $scadenze->push($fat);
        }





        return view('scadenze.index-passate',['scadenze'=>$scadenze]);
    }



}

