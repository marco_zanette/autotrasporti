<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class RimorchiController extends Controller
{



    public function index(Request $request)
    {

        $rimorchi = \App\Models\Rimorchio::all();


        return view('rimorchi.index',['rimorchi'=>$rimorchi]);
    }





    public function add(Request $request)
    {

        if($request->isMethod('post')) {

            // controllo duplicato
            $checkRecord = \App\Models\Rimorchio::where("targa",$request->input('targa'))->first();
            if($checkRecord){
                \App\Utilities\AlertMsg::setMsg('Rimorchio già presente',\App\Utilities\AlertMsg::TIPO_ERROR);
                return redirect()->back();
            }

            $rimorchioOBJ = new \App\Models\Rimorchio();
            $rimorchioOBJ->targa = $request->input('targa');
            $rimorchioOBJ->nome = $request->input('nome');
            $rimorchioOBJ->id_azienda = (int)$request->input('id_azienda');
            $rimorchioOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Aggiunto rimorchio');

            return redirect()->route('rimorchi::index');
        }


        return view('rimorchi.add');
    }






    public function edit(Request $request,$id)
    {

        $rimorchioOBJ = \App\Models\Rimorchio::find($id);


        if($request->isMethod('post')) { // gestisco post form

            $rimorchioOBJ->targa = $request->input('targa');
            $rimorchioOBJ->nome = $request->input('nome');
            $rimorchioOBJ->id_azienda = (int)$request->input('id_azienda');
            $rimorchioOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Rimorchio modificato');

            return redirect()->refresh();
        }


        return view('rimorchi.edit',['rimorchio'=>$rimorchioOBJ]);

    }


    public function delete(Request $request, $id){

        \App\Models\Rimorchio::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Rimorchio eliminato');

        return redirect()->route('rimorchi::index');


    }



}

