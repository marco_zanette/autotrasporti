<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use PDF;




class FattureController extends Controller
{



    public function index(Request $request)
    {

        $fatture = \App\Models\Fattura::all();


        return view('fatture.index',['fatture'=>$fatture]);
    }





    public function add(Request $request)
    {

        if($request->isMethod('post')) {

            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));

            $numFat = 1;
            $ultimaFat = \App\Models\Fattura::where("data",">=",$dataok->format("Y")."-01-01")
                ->where("data","<=",$dataok->format("Y")."-12-31")
                ->first();
            if($ultimaFat){
                $numFat = $ultimaFat->numero+1;
            }


            $fatOBJ = new \App\Models\Fattura();
            $fatOBJ->data = $dataok;
            $fatOBJ->numero = $numFat;
            $fatOBJ->id_azienda = $request->input('id_azienda');
            $fatOBJ->viaggi = $request->input('viaggi');
            $fatOBJ->imponibile = $request->input('imponibile');
            $fatOBJ->iva_applicata = $request->input('iva_applicata');
            $fatOBJ->iva = $request->input('iva');
            $fatOBJ->totale = (float)$request->input('totale');

            $fatOBJ->descrizione = $request->input('descrizione');


            /*
            $fatOBJ->tipo_riferimento = $request->input('tipo_riferimento');
            $fatOBJ->numero_riferimento = $request->input('numero_riferimento');
            */

            $fatOBJ->tipo_riferimento = '';
            $fatOBJ->numero_riferimento = 0;


            if($request->has('scadenza1')){
                $scad1 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza1"));
                $fatOBJ->scadenza1 = $scad1;
            }
            $fatOBJ->scadenza1_importo = (float)$request->input('scadenza1_importo');


            if($request->has('scadenza2')){
                $scad2 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza2"));
                $fatOBJ->scadenza2 = $scad2;
            }
            $fatOBJ->scadenza2_importo = (float)$request->input('scadenza2_importo');



            if($request->has('scadenza3')){
                $scad3 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza3"));
                $fatOBJ->scadenza3 = $scad3;
            }
            $fatOBJ->scadenza3_importo = (float)$request->input('scadenza3_importo');

            //$fatOBJ->scadenza4 = "";
            $fatOBJ->scadenza4_importo = 0;

            //$fatOBJ->scadenza5 = "";
            $fatOBJ->scadenza5_importo = 0;

            $fatOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Fattura inserita');

            return redirect()->route('fatture::index');

        }


        return view('fatture.add');
    }






    public function edit(Request $request,$id)
    {

        $fatOBJ = \App\Models\Fattura::find($id);


        if($request->isMethod('post')) { // gestisco post form


            $dataok = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("data"));

            $fatOBJ->data = $dataok;
            $fatOBJ->id_azienda = $request->input('id_azienda');
            $fatOBJ->viaggi = $request->input('viaggi');
            $fatOBJ->imponibile = $request->input('imponibile');
            $fatOBJ->iva_applicata = $request->input('iva_applicata');
            $fatOBJ->iva = $request->input('iva');
            $fatOBJ->totale = (float)$request->input('totale');

            $fatOBJ->descrizione = $request->input('descrizione');

            /*
            $fatOBJ->tipo_riferimento = $request->input('tipo_riferimento');
            $fatOBJ->numero_riferimento = $request->input('numero_riferimento');
            */
            $fatOBJ->tipo_riferimento = '';
            $fatOBJ->numero_riferimento = 0;

            if($request->has('scadenza1')){
                $scad1 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza1"));
                $fatOBJ->scadenza1 = $scad1;
            }
            $fatOBJ->scadenza1_importo = (float)$request->input('scadenza1_importo');


            if($request->has('scadenza2')){
                $scad2 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza2"));
                $fatOBJ->scadenza2 = $scad2;
            }
            $fatOBJ->scadenza2_importo = (float)$request->input('scadenza2_importo');



            if($request->has('scadenza3')){
                $scad3 = \Carbon\Carbon::createFromFormat("d/m/Y",$request->input("scadenza3"));
                $fatOBJ->scadenza3 = $scad3;
            }
            $fatOBJ->scadenza3_importo = (float)$request->input('scadenza3_importo');

            //$fatOBJ->scadenza4 = "";
            $fatOBJ->scadenza4_importo = 0;

            //$fatOBJ->scadenza5 = "";
            $fatOBJ->scadenza5_importo = 0;


            $fatOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Fattura modificata');

            return redirect()->refresh();

        }


        return view('fatture.edit',['fattura'=>$fatOBJ]);

    }










    public function pdf(Request $request,$id)
    {

        $fatOBJ = \App\Models\Fattura::find($id);






        $pdf = PDF::loadView('pdf.invoice', $fatOBJ);
        return $pdf->stream('document.pdf');





        return view('fatture.edit',['fattura'=>$fatOBJ]);

    }





    public function delete(Request $request, $id){

        \App\Models\Fattura::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Fattura eliminata');

        return redirect()->route('fatture::index');


    }


}

