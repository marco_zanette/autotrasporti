<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;



class LuoghiController extends Controller
{



    public function index(Request $request)
    {

        $luoghi = \App\Models\Luogo::all();


        return view('luoghi.index',['luoghi'=>$luoghi]);
    }





    public function add(Request $request)
    {

        if($request->isMethod('post')) {

            $luogoOBJ = new \App\Models\Luogo();
            $luogoOBJ->nome = $request->input('nome');
            $luogoOBJ->indirizzo = $request->input('indirizzo');
            $luogoOBJ->comune = $request->input('comune');
            $luogoOBJ->cap = $request->input('cap');
            $luogoOBJ->provincia = $request->input('provincia');
            $luogoOBJ->nazione = $request->input('nazione');
            $luogoOBJ->id_azienda = (int)$request->input('id_azienda');
            $luogoOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Luogo inserito');

            return redirect()->route('luoghi::index');

        }


        return view('luoghi.add');
    }






    public function edit(Request $request,$id)
    {

        $luogoOBJ = \App\Models\Luogo::find($id);


        if($request->isMethod('post')) { // gestisco post form


            $luogoOBJ->nome = $request->input('nome');
            $luogoOBJ->indirizzo = $request->input('indirizzo');
            $luogoOBJ->comune = $request->input('comune');
            $luogoOBJ->cap = $request->input('cap');
            $luogoOBJ->provincia = $request->input('provincia');
            $luogoOBJ->nazione = $request->input('nazione');
            $luogoOBJ->id_azienda = $request->input('id_azienda');
            $luogoOBJ->save();

            \App\Utilities\AlertMsg::setMsg('Luogo modificato');

            return redirect()->refresh();

        }


        return view('luoghi.edit',['luogo'=>$luogoOBJ]);

    }







    public function delete(Request $request, $id){

        \App\Models\Luogo::destroy($id);

        \App\Utilities\AlertMsg::setMsg('Luogo eliminato');

        return redirect()->route('luoghi::index');


    }






    public function duplicate(Request $request, $id){


        $obj_or = \App\Models\Luogo::find($id);
        $obj_new = $obj_or->replicate();
        $obj_new->save();


        \App\Utilities\AlertMsg::setMsg('Luogo duplicato');


        return redirect()->route('luoghi::edit',['id'=>$obj_new->id]);

    }


}

