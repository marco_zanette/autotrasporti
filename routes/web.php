<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/




Route::get('/pippo', function () {

    echo "agsfgsag";


    if(DB::connection()->getDatabaseName())
    {
        echo "connected successfully to database ".DB::connection()->getDatabaseName();
    }

    //return view('welcome');


});





Route::get('/demotemplate', function () {


    return view('demo.demo1');


});





Route::match(['get','post'],'/', [ 'as' => 'login', 'uses' => 'GenericoController@login']);




Route::match(['get'],'/dashboard', [ 'as' => 'dashboard', 'uses' => 'GenericoController@dashboard','middleware'=>['CheckLogin']]);







Route::group(['as' => 'veicoli::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/veicoli/', [ 'as' => 'index', 'uses' => 'VeicoliController@index']);

    Route::match(['get','post'],'/veicoli/add', [ 'as' => 'add', 'uses' => 'VeicoliController@add']);

    Route::match(['get','post'],'/veicoli/edit/{id}', [ 'as' => 'edit', 'uses' => 'VeicoliController@edit']);

    Route::match(['get','post'],'/veicoli/delete/{id}', [ 'as' => 'delete', 'uses' => 'VeicoliController@delete']);

});





Route::group(['as' => 'rimorchi::','middleware'=>['CheckLogin']], function () {


    Route::match(['get','post'],'/rimorchi/', [ 'as' => 'index', 'uses' => 'RimorchiController@index']);

    Route::match(['get','post'],'/rimorchi/add', [ 'as' => 'add', 'uses' => 'RimorchiController@add']);

    Route::match(['get','post'],'/rimorchi/edit/{id}', [ 'as' => 'edit', 'uses' => 'RimorchiController@edit']);

    Route::match(['get','post'],'/rimorchi/delete/{id}', [ 'as' => 'delete', 'uses' => 'RimorchiController@delete']);

});

Route::group(['as' => 'rifornimenti::','middleware'=>['CheckLogin']], function () {


    Route::match(['get','post'],'/rifornimenti/', [ 'as' => 'index', 'uses' => 'RifornimentiController@index']);

    Route::match(['get','post'],'/rifornimenti/add', [ 'as' => 'add', 'uses' => 'RifornimentiController@add']);

    Route::match(['get','post'],'/rifornimenti/edit/{id}', [ 'as' => 'edit', 'uses' => 'RifornimentiController@edit']);

    Route::match(['get','post'],'/rifornimenti/delete/{id}', [ 'as' => 'delete', 'uses' => 'RifornimentiController@delete']);

});


Route::group(['as' => 'aziende::','middleware'=>['CheckLogin']], function () {

    Route::match(['get','post'],'/aziende/', [ 'as' => 'index', 'uses' => 'AziendeController@index']);

    Route::match(['get','post'],'/aziende/add', [ 'as' => 'add', 'uses' => 'AziendeController@add']);

    Route::match(['get','post'],'/aziende/edit/{id}', [ 'as' => 'edit', 'uses' => 'AziendeController@edit']);

    Route::match(['get','post'],'/aziende/delete/{id}', [ 'as' => 'delete', 'uses' => 'AziendeController@delete']);

});






Route::group(['as' => 'viaggi::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/viaggi/', [ 'as' => 'index', 'uses' => 'ViaggiController@index']);

    Route::match(['get','post'],'/viaggi/add', [ 'as' => 'add', 'uses' => 'ViaggiController@add']);

    Route::match(['get','post'],'/viaggi/edit/{id}', [ 'as' => 'edit', 'uses' => 'ViaggiController@edit']);

    Route::match(['get','post'],'/viaggi/delete/{id}', [ 'as' => 'delete', 'uses' => 'ViaggiController@delete']);

    Route::match(['get','post'],'/viaggi/duplicate/{id}', [ 'as' => 'duplicate', 'uses' => 'ViaggiController@duplicate']);


    Route::match(['get','post'],'/viaggi/scarico-edit/{id}', [ 'as' => 'scarico-edit', 'uses' => 'ViaggiController@scaricoEdit']);

    Route::match(['get','post'],'/viaggi/carico-edit/{id}', [ 'as' => 'carico-edit', 'uses' => 'ViaggiController@caricoEdit']);


    Route::match(['get','post'],'/viaggi/scarico-delete/{id}', [ 'as' => 'scarico-delete', 'uses' => 'ViaggiController@scaricoDelete']);

    Route::match(['get','post'],'/viaggi/carico-delete/{id}', [ 'as' => 'carico-delete', 'uses' => 'ViaggiController@caricoDelete']);

    Route::match(['get','post'],'/viaggi/riferimento-delete/{id}', [ 'as' => 'riferimento-delete', 'uses' => 'ViaggiController@riferimentoDelete']);

    Route::match(['get','post'],'/viaggi/riferimento-edit/{id}', [ 'as' => 'riferimento-edit', 'uses' => 'ViaggiController@riferimentoEdit']);


});

Route::group(['as' => 'luoghi::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/luoghi/', [ 'as' => 'index', 'uses' => 'LuoghiController@index']);

    Route::match(['get','post'],'/luoghi/add', [ 'as' => 'add', 'uses' => 'LuoghiController@add']);

    Route::match(['get','post'],'/luoghi/edit/{id}', [ 'as' => 'edit', 'uses' => 'LuoghiController@edit']);

    Route::match(['get','post'],'/luoghi/delete/{id}', [ 'as' => 'delete', 'uses' => 'LuoghiController@delete']);

    Route::match(['get','post'],'/luoghi/duplicate/{id}', [ 'as' => 'duplicate', 'uses' => 'LuoghiController@duplicate']);

});




Route::group(['as' => 'fatture::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/fatture/', [ 'as' => 'index', 'uses' => 'FattureController@index']);

    Route::match(['get','post'],'/fatture/add', [ 'as' => 'add', 'uses' => 'FattureController@add']);

    Route::match(['get','post'],'/fatture/edit/{id}', [ 'as' => 'edit', 'uses' => 'FattureController@edit']);

    Route::match(['get','post'],'/fatture/pdf/{id}', [ 'as' => 'pdf', 'uses' => 'FattureController@pdf']);

    Route::match(['get','post'],'/fatture/delete/{id}', [ 'as' => 'delete', 'uses' => 'FattureController@delete']);

});




Route::group(['as' => 'ordini::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/ordini/', [ 'as' => 'index', 'uses' => 'OrdiniController@index']);

    Route::match(['get','post'],'/ordini/add', [ 'as' => 'add', 'uses' => 'OrdiniController@add']);

    Route::match(['get','post'],'/ordini/edit/{id}', [ 'as' => 'edit', 'uses' => 'OrdiniController@edit']);

    Route::match(['get','post'],'/ordini/duplicate/{id}', [ 'as' => 'duplicate', 'uses' => 'OrdiniController@duplicate']);

    Route::match(['get','post'],'/ordini/pdf/{id}', [ 'as' => 'pdf', 'uses' => 'OrdiniController@pdf']);

    Route::match(['get','post'],'/ordini/delete/{id}', [ 'as' => 'delete', 'uses' => 'OrdiniController@delete']);

});







Route::group(['as' => 'fattureacquisto::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/fattureacquisto/', [ 'as' => 'index', 'uses' => 'FattureAcquistoController@index']);

    Route::match(['get','post'],'/fattureacquisto/add', [ 'as' => 'add', 'uses' => 'FattureAcquistoController@add']);

    Route::match(['get','post'],'/fattureacquisto/edit/{id}', [ 'as' => 'edit', 'uses' => 'FattureAcquistoController@edit']);

    Route::match(['get','post'],'/fattureacquisto/delete/{id}', [ 'as' => 'delete', 'uses' => 'FattureAcquistoController@delete']);

});






Route::group(['as' => 'scadenze::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/scadenze/', [ 'as' => 'index', 'uses' => 'ScadenzeController@index']);

    Route::match(['get'],'/scadenze-passate/', [ 'as' => 'index-passate', 'uses' => 'ScadenzeController@scadenzePassate']);


});





Route::group(['as' => 'viaggivuoti::','middleware'=>['CheckLogin']], function () {


    Route::match(['get'],'/viaggivuoti/', [ 'as' => 'index', 'uses' => 'ViaggivuotiController@index']);

    Route::match(['get','post'],'/viaggivuoti/add', [ 'as' => 'add', 'uses' => 'ViaggivuotiController@add']);

    Route::match(['get','post'],'/viaggivuoti/edit/{id}', [ 'as' => 'edit', 'uses' => 'ViaggivuotiController@edit']);

    Route::match(['get','post'],'/viaggivuoti/delete/{id}', [ 'as' => 'delete', 'uses' => 'ViaggivuotiController@delete']);

});